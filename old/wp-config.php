<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'sonja');

/** MySQL database username */
define('DB_USER', 'sonja');

/** MySQL database password */
define('DB_PASSWORD', '5GNnxEZ5e3GWUEEn');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'A0RQu,{>:pw;,kgPY|k ^*3)l/M|%0UNz1H;OS?Z6.(}iY7w%(^Irves-ocIgTC.');
define('SECURE_AUTH_KEY',  'u_*]hA&*tVC#uJXg-+K)x4AoM!e ni+r*Z+;XA5aKM;+t/vwr)HQXnK;<@TqVw-j');
define('LOGGED_IN_KEY',    ':`g(u.jnkt:#go5LzTZ/@8p18)gGku[<=(?M4Ro&b3{y#.<s?%[fs3313}ME@vC3');
define('NONCE_KEY',        '%XtG1Or7Ww]dM$]c:QZE+<JjLvr4_Ub0Txj$2WT@$!45cr_^-5ZL>nR<xxyH4($T');
define('AUTH_SALT',        '+6.c(Fccs&GuVATH=2F{D/?I04Uhy<X33ejD^Idz]k-oIH]5!i+_[Reo+@bxV./I');
define('SECURE_AUTH_SALT', 'Dj=Dhmz2ls4L*D!AvzZ]}@/Up)+5.~2i)H/Fh[3&B%oauK60pI~-An`)$_7=:G 9');
define('LOGGED_IN_SALT',   '=[s!rSXo2b9%O`x!n}krD:3D]`[i[b_v~9nJ,3>!uE|4WO%^vR/bIgyt}c~Ao%P[');
define('NONCE_SALT',       'b9A:H7CVEdOxoL?x0l@eqWpO,,dxGb}*s(b SM~}y[=0yxgW^egTR{+kNVob_Q,v');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
