
;(function($){
    $(function(){
        

        // Make slider items sortable.
        var slider = $('#owlabkbs-output');

        // Use ajax to make the images sortable.
        slider.sortable({
            containment: '#owlabkbs',
            items: 'li',
            cursor: 'move',
            forcePlaceholderSize: true,
            placeholder: 'dropzone',
            update: function(event, ui) {
                // Make ajax request to sort out items.
                var opts = {
                    url:      owlabkbs_metabox.ajax,
                    type:     'post',
                    async:    true,
                    cache:    false,
                    dataType: 'json',
                    data: {
                        action:  'owlabkbs_sort_images',
                        order:   slider.sortable('toArray').toString(),
                        post_id: owlabkbs_metabox.id,
                        nonce:   owlabkbs_metabox.sort
                    },
                    success: function(response) {
                        return;
                    },
                    error: function(xhr, textStatus ,e) {
                        return;
                    }
                };
                $.ajax(opts);
            }
        });

        // Process image removal from a slider.
        $(document).on('click', '#owlabkbs .owlabkbs-remove-slide', function(e){
            e.preventDefault();

            // Bail out if the user does not actually want to remove the image.
            var confirm_delete = confirm(owlabkbs_metabox.remove);
            if ( ! confirm_delete )
                return;

            // Prepare our data to be sent via Ajax.
            var attach_id = $(this).parent().attr('id'),
                remove = {
                    action:        'owlabkbs_remove_slide',
                    attachment_id: attach_id,
                    post_id:       owlabkbs_metabox.id,
                    nonce:         owlabkbs_metabox.remove_nonce
                };

            // Process the Ajax response and output all the necessary data.
            $.post(
                owlabkbs_metabox.ajax,
                remove,
                function(response) {
                    $('#' + attach_id).fadeOut('normal', function() {
                        $(this).remove();

                        // Refresh the modal view to ensure no items are still checked if they have been removed.
                        $('.owlabkbs-load-library').attr('data-owlabkbs-offset', 0).addClass('has-search').trigger('click');
                    });
                },
                'json'
            );
        });

        // Open up the media modal area for modifying slider metadata.
        var owlabkbs_main_frame_meta = false;
        $(document).on('click.owlabkbsModify', '#owlabkbs .owlabkbs-modify-slide', function(e){
            e.preventDefault();
            var attach_id = $(this).parent().data('owlabkbs-slide'),
                formfield = 'owlabkbs-meta-' + attach_id;

            // Show the modal.
            owlabkbs_main_frame_meta = true;
            $('#' + formfield).appendTo('body').show();

            

            // Close the modal window on user action
            var append_and_hide_meta = function(e){
                e.preventDefault();
                $('#' + formfield).appendTo('#' + attach_id).hide();
                owlabkbs_main_frame_meta = false;
                $(document).off('click.owlabkbsLink');
            };
            $(document).on('click.owlabkbsIframe', '.media-modal-close, .media-modal-backdrop', append_and_hide_meta);
            $(document).off('keydown.owlabkbsIframe').on('keydown.owlabkbsIframe', function(e){
                if ( 27 == e.keyCode && owlabkbs_main_frame_meta ) {
                    append_and_hide_meta(e);
                }
            });
            $(document).on('click.owlabkbsLink', '.ed_button', function(){
                // Set custom z-index for link dialog box.
                $('#wp-link-backdrop').css('zIndex', '170100');
                $('#wp-link-wrap').css('zIndex', '171005' );
            });
        });

        // Save the slider metadata.
        $(document).on('click', '.owlabkbs-meta-submit', function(e){
            e.preventDefault();
            var $this     = $(this),
                default_t = $this.text(),
                attach_id = $this.data('owlabkbs-item'),
                formfield = 'owlabkbs-meta-' + attach_id,
                meta      = {};

            // Output saving text...
            $this.text(owlabkbs_metabox.saving);

            // Add the title since it is a special field.
            meta.caption = $('#owlabkbs-meta-table-' + attach_id).find('textarea[name="_owlabkbs[meta_caption]"]').val();

            // Get all meta fields and values.
            $('#owlabkbs-meta-table-' + attach_id).find(':input').not('.ed_button').each(function(i, el){
                if ( $(this).data('owlabkbs-meta') )
                    meta[$(this).data('owlabkbs-meta')] = $(this).val();
            });

            // Prepare the data to be sent.
            var data = {
                action:    'owlabkbs_save_meta',
                nonce:     owlabkbs_metabox.save_nonce,
                attach_id: attach_id,
                post_id:   owlabkbs_metabox.id,
                meta:      meta
            };

            $.post(
                owlabkbs_metabox.ajax,
                data,
                function(res){
                    setTimeout(function(){
                        $('#' + formfield).appendTo('#' + attach_id).hide();
                        $this.text(default_t);
                    }, 500);
                },
                'json'
            );
        });

        
    });
}(jQuery));