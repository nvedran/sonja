var owlabbulkg_file_frame;
jQuery(function($){
	

  //Open the WP media library on click  
  $('.owlabbulkg-media-wp-library').live('click', function( event ){

    event.preventDefault();


    // If the media frame already exists, reopen it.
    if ( owlabbulkg_file_frame ) {
      owlabbulkg_file_frame.open();
      return;
    }

    // Create the media frame.
    owlabbulkg_file_frame = wp.media.frames.file_frame = wp.media({
      title: owl_uploader_obj.title,
      button: {
        text: owl_uploader_obj.button_text
      },
      multiple: true  // Set to true to allow multiple files to be selected
    });

    //on open
    owlabbulkg_file_frame.on( 'open', function() {

      var selection = owlabbulkg_file_frame.state().get('selection');
      
      //get IDs of already selected images
      var IDs=[];
      $.each($("#owlabbulkg-output > li.owlabbulkg-slide"),function(){
        IDs.push($(this).data('owlabbulkg-slide'));
      });

      //add these to the selection list
      IDs.forEach(function(id) {
        attachment = wp.media.attachment(id);
        selection.add( attachment ? [ attachment ] : [] );
      });

    });



    // When an image is selected, run a callback.
    owlabbulkg_file_frame.on( 'select', function() {

        var data  = {
            action: 'owlabbulkg_insert_slides',
            nonce:   owlabbulkg_metabox.insert_nonce,
            post_id: owlabbulkg_metabox.id,
            images:  {},
            videos:  {},
            html:    {}
        }

        var selection = owlabbulkg_file_frame.state().get('selection');

        selection.map( function( attachment ,i) {
     
          attachment = attachment.toJSON();

          data.images[i]=attachment.id;
        });

        //Add new images
        owlabbulkg_media_make_ajax_call(data,function(){
            //Update the current images UI
            var data = {
                action:  'owlabbulkg_refresh',
                post_id: owlabbulkg_metabox.id,
                nonce:   owlabbulkg_metabox.refresh_nonce
             };
            owlabbulkg_media_make_ajax_call(data,function(res){
                if ( res && res.success ) {
                    $('#owlabbulkg-output').html(res.success);
                    
                }
            });
        });
     
    });


    function owlabbulkg_media_make_ajax_call(data,callback){
        
        $.post(
            owlabbulkg_metabox.ajax,
            data,
            function(response){
                callback(response);
            },
            'json'
        );
      
    }

    // Finally, open the modal
    owlabbulkg_file_frame.open();
  });

});