<?php
/**
 * Posttype admin class.
 *
 * @since 1.0.0
 *
 * @package owwwlab-kenburn
 * @author  owwwlab
 */
class Owlabbulkg_Posttype_Admin {

    /**
     * Holds the class object.
     *
     * @since 1.0.0
     *
     * @var object
     */
    public static $instance;

    /**
     * Path to the file.
     *
     * @since 1.0.0
     *
     * @var string
     */
    public $file = __FILE__;

    /**
     * Holds the base class object.
     *
     * @since 1.0.0
     *
     * @var object
     */
    public $base;

    /**
     * Primary class constructor.
     *
     * @since 1.0.0
     */
    public function __construct() {

        // Load the base class object.
        $this->base = Owlabbulkg::get_instance();

        // Remove quick editing from the post type row actions.
        add_filter( 'post_row_actions', array( $this, 'row_actions' ), 10, 2 );

        // Manage post type columns.
        add_filter( 'manage_edit-owlabbulkg_columns', array( $this, 'owlabbulkg_columns' ) );
        add_filter( 'manage_owlabbulkg_posts_custom_column', array( $this, 'owlabbulkg_custom_columns' ), 10, 2 );

        // Update post type messages.
        add_filter( 'post_updated_messages', array( $this, 'messages' ) );

        // Force the menu icon to be scaled to proper size (for Retina displays).
        add_action( 'admin_head', array( $this, 'menu_icon' ) );

    }


    /**
     * Filter out unnecessary row actions from the post table.
     *
     * @since 1.0.0
     *
     * @param array $actions  Default row actions.
     * @param object $post    The current post object.
     * @return array $actions filter for row actions.
     */
    public function row_actions( $actions, $post ) {

        if ( isset( get_current_screen()->post_type ) && 'owlabbulkg' == get_current_screen()->post_type ) {
            unset( $actions['inline hide-if-no-js'] );
        }

        return apply_filters( 'owlabbulkg_row_actions', $actions, $post );

    }

    /**
     * Customize the post columns for the owlabbulkg post type.
     *
     * @since 1.0.0
     *
     * @param array $columns  The default columns.
     * @return array $columns Amended columns.
     */
    public function owlabbulkg_columns( $columns ) {

        $columns = $columns + array(
            'images'    => __( 'Number of Images', 'owlabbulkg' ),
            'cover'    => __( 'Cover Image', 'owlabbulkg' ),
            'gallery-category' => __('Category','owlabbulkg'),
        );
       

        return $columns;

    }

    /**
     * Add data to the custom columns added to the owlabbulkg post type.
     *
     * @since 1.0.0
     *
     * @global object $post  The current post object.
     * @param string $column The name of the custom column.
     * @param int $post_id   The current post ID.
     */
    public function owlabbulkg_custom_columns( $column, $post_id ) {

        global $post;
        $post_id = absint( $post_id );

        switch ( $column ) {
           
            case 'cover' :
                echo get_the_post_thumbnail( $post->ID, array(100,100) );
                $att = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID),'full' );
                if ($att):
                    echo "<br>".__("Size","owlabbulkg").":<code>".$att[1].'x'.$att[2].'px</code>';
                else:
                    _e('No cover image set','owlabbulkg');
                endif;

                break;
            case 'images' :
                $slider_data = get_post_meta( $post_id, '_owlabbulkg_slider_data', true );
                echo ( ! empty( $slider_data['slider'] ) ? count( $slider_data['slider'] ) : 0 );
                echo "<br><code>gallery ID = $post_id</code>";
                break;
            case 'gallery-category' : 
                echo get_the_term_list( $post->ID, 'owlabbulkg_category', '', ', ',''); 
                break;

        }

    }

    

    /**
     * Contextualizes the post updated messages.
     *
     * @since 1.0.0
     *
     * @global object $post    The current post object.
     * @param array $messages  Array of default post updated messages.
     * @return array $messages Amended array of post updated messages.
     */
    public function messages( $messages ) {

        global $post;

        // Contextualize the messages.
        $messages['owlabbulkg'] = apply_filters( 'owlabbulkg_messages',
            array(
                0  => '',
                1  => __( 'Gallery updated.', 'owlabbulkg' ),
                2  => __( 'Gallery custom field updated.', 'owlabbulkg' ),
                3  => __( 'Gallery custom field deleted.', 'owlabbulkg' ),
                4  => __( 'Gallery updated.', 'owlabbulkg' ),
                5  => isset( $_GET['revision'] ) ? sprintf( __( 'Gallery restored to revision from %s.', 'owlabbulkg' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
                6  => __( 'Gallery published.', 'owlabbulkg' ),
                7  => __( 'Gallery saved.', 'owlabbulkg' ),
                8  => __( 'Gallery submitted.', 'owlabbulkg' ),
                9  => sprintf( __( 'Gallery scheduled for: <strong>%1$s</strong>.', 'owlabbulkg' ), date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ) ),
                10 => __( 'Gallery draft updated.', 'owlabbulkg' )
            )
        );

        return $messages;

    }

    /**
     * Forces the Soliloquy menu icon width/height for Retina devices.
     *
     * @since 1.0.0
     */
    public function menu_icon() {

        ?>
        <style type="text/css">#menu-posts-owlabbulkg .wp-menu-image img { width: 16px; height: 16px; }</style>
        <?php

    }









    /**
     * Returns the singleton instance of the class.
     *
     * @since 1.0.0
     *
     * @return object The owlabbulkg_Posttype_Admin object.
     */
    public static function get_instance() {

        if ( ! isset( self::$instance ) && ! ( self::$instance instanceof Owlabbulkg_Posttype_Admin ) ) {
            self::$instance = new Owlabbulkg_Posttype_Admin();
        }

        return self::$instance;

    }

}

// Load the posttype admin class.
$owlabbulkg_Posttype_Admin = Owlabbulkg_Posttype_Admin::get_instance();






