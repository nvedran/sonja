<?php
/**
 * Metabox class.
 *
 * @since 1.0.0
 *
 * @package owwwlab-kenburn
 * @author  owwwlab
 */
class Owlabbulkg_Metaboxes {

    /**
     * Holds the class object.
     *
     * @since 1.0.0
     *
     * @var object
     */
    public static $instance;

    /**
     * Path to the file.
     *
     * @since 1.0.0
     *
     * @var string
     */
    public $file = __FILE__;

    /**
     * Holds the base class object.
     *
     * @since 1.0.0
     *
     * @var object
     */
    public $base;

    /**
     * Primary class constructor.
     *
     * @since 1.0.0
     */
    public function __construct() {

        // Load the base class object.
        $this->base = Owlabbulkg::get_instance();

        // Load metabox assets.
        add_action( 'admin_enqueue_scripts', array( $this, 'meta_box_styles' ) );
        add_action( 'admin_enqueue_scripts', array( $this, 'meta_box_scripts' ) );

        // Load the metabox hooks and filters.
        add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes' ), 100 );

        // Load all tabs.
        add_action( 'owlabbulkg_tab_images', array( $this, 'images_tab' ) );
        add_action( 'owlabbulkg_tab_config', array( $this, 'config_tab' ) );
        add_action( 'owlabbulkg_tab_config_ext', array( $this, 'config_ext_tab' ) );

        // Add action to save metabox config options.
        add_action( 'save_post', array( $this, 'save_meta_boxes' ), 10, 2 );

    }

    /**
     * Loads styles for our metaboxes.
     *
     * @since 1.0.0
     *
     * @return null Return early if not on the proper screen.
     */
    public function meta_box_styles() {

        if ( 'post' !== get_current_screen()->base ) {
            return;
        }

        if ( isset( get_current_screen()->post_type ) && in_array( get_current_screen()->post_type, $this->get_skipped_posttypes() ) ) {
            return;
        }

        // Load necessary metabox styles.
        wp_register_style( $this->base->plugin_slug . '-metabox-style', plugins_url( 'assets/css/metabox.css', $this->base->file ), array(), $this->base->version );
        wp_enqueue_style( $this->base->plugin_slug . '-metabox-style' );



        // Fire a hook to load in custom metabox styles.
        do_action( 'owlabbulkg_metabox_styles' );

    }

    /**
     * Loads scripts for our metaboxes.
     *
     * @since 1.0.0
     *
     * @global int $id      The current post ID.
     * @global object $post The current post object..
     * @return null         Return early if not on the proper screen.
     */
    public function meta_box_scripts( $hook ) {

        global $id, $post;

        if ( isset( get_current_screen()->base ) && 'post' !== get_current_screen()->base ) {
            return;
        }

        if ( isset( get_current_screen()->post_type ) && in_array( get_current_screen()->post_type, $this->get_skipped_posttypes() ) ) {
            return;
        }

        // Set the post_id for localization.
        $post_id = isset( $post->ID ) ? $post->ID : (int) $id;

        // Load necessary metabox scripts.
        wp_enqueue_script( 'jquery-ui-sortable' );
        wp_enqueue_media( array( 'post' => $post_id ) );

        // Load necessary metabox scripts.
        wp_enqueue_script( 'plupload-handlers' );
        wp_register_script( $this->base->plugin_slug . '-metabox-script', plugins_url( 'assets/js/metabox.js', $this->base->file ), array( 'jquery', 'plupload-handlers', 'quicktags', 'jquery-ui-sortable' ), $this->base->version, true );
        wp_enqueue_script( $this->base->plugin_slug . '-metabox-script' );
        wp_localize_script(
            $this->base->plugin_slug . '-metabox-script',
            'owlabbulkg_metabox',
            array(
                'ajax'           => admin_url( 'admin-ajax.php' ),
                'id'             => $post_id,
                'remove'         => __( 'Are you sure you want to remove this slide from the slider?', 'owlabbulkg' ),
                'remove_nonce'   => wp_create_nonce( 'owlabbulkg-remove-slide' ),
                'save_nonce'     => wp_create_nonce( 'owlabbulkg-save-meta' ),
                'saving'         => __( 'Saving...', 'owlabbulkg' ),
                'sort'           => wp_create_nonce( 'owlabbulkg-sort' ),
                'inserting'      => __( 'Inserting...', 'owlabbulkg' ),
                'insert_nonce'   => wp_create_nonce( 'owlabbulkg-insert-images' ),
                'refresh_nonce'  => wp_create_nonce( 'owlabbulkg-refresh' ),
                
            )
        );

        


        //Added since v 1.6 
        //Loading required JS for WP media uploader and media handler
        //wp_enqueue_media();
        wp_register_script( $this->base->plugin_slug.'medial-handler',plugins_url('assets/js/owlabbulkg-admin.js', $this->base->file ), array(), $this->base->version, true );
        $translation_array = array(
            'title' => __('Insert Images', 'owlabbulkg'),
            'button_text' => __('Insert to gallery', 'owlabbulkg')
        );
        wp_localize_script( $this->base->plugin_slug.'medial-handler', 'owl_uploader_obj',$translation_array);

        wp_enqueue_script($this->base->plugin_slug.'medial-handler');

        // If on an owlabbulkg post type, add custom CSS for hiding specific things.
        if ( isset( get_current_screen()->post_type ) && 'owlabbulkg' == get_current_screen()->post_type ) {
            add_action( 'admin_head', array( $this, 'meta_box_css' ) );
        }


        // Fire a hook to load custom metabox scripts.
        do_action( 'owlabbulkg_metabox_scripts' );

    }

    

    /**
     * Hides unnecessary meta box items on owlabbulkg post type screens.
     *
     * @since 1.0.0
     */
    public function meta_box_css() {

        

        // Fire action for CSS on owlabbulkg post type screens.
        do_action( 'owlabbulkg_admin_css' );

    }

    /**
     * Creates metaboxes for handling and managing sliders.
     *
     * @since 1.0.0
     */
    public function add_meta_boxes() {

        

        // Get all public post types.
        $post_types = get_post_types( array( 'public' => true ) );

        // Splice the owlabbulkg post type since it is not visible to the public by default.
        $post_types[] = 'owlabbulkg';

        // Loops through the post types and add the metaboxes.
        foreach ( (array) $post_types as $post_type ) {
            // Don't output boxes on these post types.
            if ( in_array( $post_type, $this->get_skipped_posttypes() ) ) {
                continue;
            }

            add_meta_box( 'owlabbulkg', __( 'Gallery Settings', 'owlabbulkg' ), array( $this, 'meta_box_callback' ), $post_type, 'normal', 'high' );
        }

    }


    /**
     * Callback for displaying content in the registered metabox.
     *
     * @since 1.0.0
     *
     * @param object $post The current post object.
     */
    public function meta_box_callback( $post ) {

        // Keep security first.
        wp_nonce_field( 'owlabbulkg', 'owlabbulkg' );

        // Check for our meta overlay helper.
        $slider_data = get_post_meta( $post->ID, '_owlabbulkg_slider_data', true );
        


        ?>
        <div id="owlabbulkg-tabs" class="owlabbulkg-clear <?php echo $class; ?>">
            
            
            <?php foreach ( (array) $this->get_owlabbulkg_tab_nav() as $id => $title ) :  ?>
                
                <div id="owlabbulkg-<?php echo $id; ?>" class="owlabbulkg-clear">
                    <h2><?php echo $title; ?></h2><hr>
                    <?php do_action( 'owlabbulkg_tab_' . $id, $post ); ?>
                </div>
            <?php endforeach; ?>

            
        </div>
        <?php

    }

    /**
     * Callback for getting all of the tabs for owlabbulkg sliders.
     *
     * @since 1.0.0
     *
     * @return array Array of tab information.
     */
    public function get_owlabbulkg_tab_nav() {

        $tabs = array(
            'images'     => __( 'Images', 'owlabbulkg' ),
            'config'     => __( 'Settings for single Gallery page', 'owlabbulkg' ),
            'config_ext' => __( 'Settings for Archive page', 'owlabbulkg')
        );
        $tabs = apply_filters( 'owlabbulkg_tab_nav', $tabs );

        return $tabs;

    }

    /**
     * Callback for displaying the UI for main images tab.
     *
     * @since 1.0.0
     *
     * @param object $post The current post object.
     */
    public function images_tab( $post ) {

        

        

        // Output the display based on the type of slider being created.
        echo '<div id="owlabbulkg-slider-main" class="owlabbulkg-clear">';
            $this->images_display( $this->get_config( 'type', $this->get_config_default( 'type' ) ), $post );
        echo '</div>';

    }

    /**
     * Returns the types of sliders available.
     *
     * @since 1.0.0
     *
     * @param object $post The current post object.
     * @return array       Array of slider types to choose.
     */
    public function get_owlabbulkg_types( $post ) {

        $types = array(
            'default' => __( 'Default', 'owlabbulkg' )
        );

        return apply_filters( 'owlabbulkg_slider_types', $types, $post );

    }

    /**
     * Determines the Images tab display based on the type of slider selected.
     *
     * @since 1.0.0
     *
     * @param string $type The type of display to output.
     * @param object $post The current post object.
     */
    public function images_display( $type = 'default', $post ) {

        // Output a unique hidden field for settings save testing for each type of slider.
        echo '<input type="hidden" name="_owlabbulkg[type_' . $type . ']" value="1" />';

        // Output the display based on the type of slider available.
        switch ( $type ) {
            case 'default' :
                $this->do_default_display( $post );
                break;
            default:
                do_action( 'owlabbulkg_display_' . $type, $post );
                break;
        }

    }

    /**
     * Callback for displaying the default slider UI.
     *
     * @since 1.0.0
     *
     * @param object $post The current post object.
     */
    public function do_default_display( $post ) {

        

        // Output the custom media upload form.
        //owlabbulkg_Media::get_instance()->media_upload_form();

        // Prepare output data.
        $slider_data = get_post_meta( $post->ID, '_owlabbulkg_slider_data', true );

        ?>
        <div id="owlabbulkg-media-library-wrapper">
            <a class="owlabbulkg-media-wp-library button button-primary" href="#" title="<?php _e( 'Insert From media library', 'owlabbulkg' ) ?>"><?php _e( 'Insert From media library', 'owlabbulkg' ) ?></a>
        </div>
        <ul id="owlabbulkg-output" class="owlabbulkg-clear">
            <?php if ( ! empty( $slider_data['slider'] ) ) : ?>
                <?php foreach ( $slider_data['slider'] as $id => $data ) : ?>
                    <?php echo $this->get_slider_item( $id, $data, ( ! empty( $data['type'] ) ? $data['type'] : 'image' ), $post->ID ); ?>
                <?php endforeach; ?>
            <?php endif; ?>
        </ul>
        <?php 

    }



    /**
     * Callback for displaying the UI for setting slider config options.
     *
     * @since 1.0.0
     *
     * @param object $post The current post object.
     */
    public function config_tab( $post ) {

        ?>
        <div id="owlabbulkg-config">
            <p class="owlabbulkg-intro"><?php _e( 'The settings below adjust the basic configuration options for the Gallery display.', 'owlabbulkg' ); ?></p>
            <table class="form-table">
                <tbody>
                    
                    <tr id="owlabbulkg-config-slider-hover-box">
                        <th scope="row">
                            <label for="owlabbulkg-config-hover"><?php _e( 'Image Hover Effect', 'owlabbulkg' ); ?></label>
                        </th>
                        <td>
                            <select id="owlabbulkg-config-hover" name="_owlabbulkg[hover]">
                              <?php $this->get_gallery_hovers(); ?>  
                            </select>
                        </td>
                    </tr>

                    <tr id="owlabbulkg-config-slider-icon-box">
                        <th scope="row">
                            <label for="owlabbulkg-config-icon"><?php _e( 'Fontawesome icon classname', 'owlabbulkg' ); ?></label>
                        </th>
                        <td>
                            <input type="text" name="_owlabbulkg[icon]" value="<?php echo $this->get_config( 'icon', $this->get_config_default( 'icon' )); ?>" >
                            <p class="description"><?php _e('Find availabe icons <a href="http://fortawesome.github.io/Font-Awesome/icons/">here</a> eg:<code>fa-heart</code>') ?></p>
                        </td>
                    </tr>

                    <tr id="owlabbulkg-config-slider-sidebar-box">
                        <th scope="row">
                            <label for="owlabbulkg-config-sidebar"><?php _e( 'Show Sidebar?', 'owlabbulkg' ); ?></label>
                        </th>
                        <td>
                            <?php 
                            $checked = '';
                            if ( $this->get_config( 'sidebar', $this->get_config_default( 'sidebar' ) ) == 'on' ){
                                $checked = 'checked';
                            } ?>
                            <input type="checkbox" name="_owlabbulkg[sidebar]" <?php echo $checked; ?>><?php echo __('Yes, please','owlabbulkg'); ?>
                        </td>
                    </tr>

                    <tr id="owlabbulkg-config-slider-sidebar-content-box">
                        <th scope="row">
                            <label for="owlabbulkg-config-sidebar-content"><?php _e( 'Sidebar Content', 'owlabbulkg' ); ?></label>
                        </th>
                        <td>
                            <textarea name="_owlabbulkg[sidebar_content]" rows="4"><?php echo $this->get_config( 'sidebar_content', $this->get_config_default( 'sidebar_content' )); ?></textarea>
                            <p class="description"><?php _e('Any text or HTML content','owlabbulkg') ?></p>
                        </td>
                    </tr>

                     <tr id="owlabbulkg-config-slider-layout-box">
                        <th scope="row">
                            <label for="owlabbulkg-config-layout"><?php _e( 'Layout type', 'owlabbulkg' ); ?></label>
                        </th>
                        <td>
                            <select id="owlabbulkg-config-layout" name="_owlabbulkg[layout]">
                              <?php $this->get_gallery_layouts(); ?>  
                            </select>
                        </td>
                    </tr>

                    

                    <tr id="owlabbulkg-config-slider-col-box">
                        <th scope="row">
                            <label for="owlabbulkg-config-lg-col"><?php _e( 'Number of columns for Grid', 'owlabbulkg' ); ?></label>
                        </th>
                        <td>
                            <?php _e('Large screen devices','owlabbulkg') ?>  :<br> <input type="number" class="owlabbulkg-number-input" name="_owlabbulkg[lg_col]" value="<?php echo $this->get_config( 'lg_col', $this->get_config_default( 'lg_col' )); ?>" ><br>
                            <?php _e('Medium screen devides','owlabbulkg') ?> :<br> <input type="number" class="owlabbulkg-number-input" name="_owlabbulkg[md_col]" value="<?php echo $this->get_config( 'md_col', $this->get_config_default( 'md_col' )); ?>" ><br>
                            <?php _e('Small screen devides','owlabbulkg') ?>  :<br> <input type="number" class="owlabbulkg-number-input" name="_owlabbulkg[sm_col]" value="<?php echo $this->get_config( 'sm_col', $this->get_config_default( 'sm_col' )); ?>" ><br>
                            <?php _e('Mobile screen devides','owlabbulkg') ?> :<br> <input type="number" class="owlabbulkg-number-input" name="_owlabbulkg[xs_col]" value="<?php echo $this->get_config( 'xs_col', $this->get_config_default( 'xs_col' )); ?>" ><br>
                        </td>
                    </tr>

                    <tr id="owlabbulkg-config-slider-nopadding-box">
                        <th scope="row">
                            <label for="owlabbulkg-config-nopadding"><?php _e( 'Remove spaces between images?', 'owlabbulkg' ); ?></label>
                        </th>
                        <td>
                            <?php 
                            $checked = '';
                            if ( $this->get_config( 'nopadding', $this->get_config_default( 'nopadding' ) ) == 'on' ){
                                $checked = 'checked';
                            } ?>
                            <input type="checkbox" name="_owlabbulkg[nopadding]" <?php echo $checked; ?>><?php echo __('Yes, please','owlabbulkg'); ?>
                        </td>
                    </tr>

                    <tr id="owlabbulkg-config-slider-ratio-box">
                        <th scope="row">
                            <label for="owlabbulkg-config-ratio"><?php _e( 'Set Same Ratio images on Grid?', 'owlabbulkg' ); ?></label>
                        </th>
                        <td>
                            <?php 
                            $checked = '';
                            if ( $this->get_config( 'same_ratio', $this->get_config_default( 'same_ratio' ) ) == 'on' ){
                                $checked = 'checked';
                            } ?>
                            <input type="checkbox" name="_owlabbulkg[same_ratio]" <?php echo $checked; ?>><?php echo __('Yes, please','owlabbulkg'); ?>
                        </td>
                    </tr>

                    
                    <?php do_action( 'owlabbulkg_config_box', $post ); ?>
                </tbody>
            </table>
            
        </div>
        <?php

    }

    public function config_ext_tab($post){
        ?>
        <div id="owlabbulkg-config_ext">
            <p class="owlabbulkg-intro"><?php _e( 'The settings below adjust the display of cover image of this galley at archive page.', 'owlabbulkg' ); ?></p>
            <table class="form-table">
                <tbody>

                    <tr id="owlabbulkg-config-slider-short-des-box">
                        <th scope="row">
                            <label for="owlabbulkg-config-short-des"><?php _e( 'Short description of gallery', 'owlabbulkg' ); ?></label>
                        </th>
                        <td>
                            <textarea name="_owlabbulkg[short_des]" rows="4"><?php echo $this->get_config( 'short_des', $this->get_config_default( 'short_des' )); ?></textarea>
                        </td>

                    </tr>

                    <tr id="owlabbulkg-config-slider-ratio-box">
                        <th scope="row">
                            <label for="owlabbulkg-config-ratio"><?php _e( 'Ratio of Cover Image in Archive page Grid layout', 'owlabbulkg' ); ?></label>
                        </th>
                        <td>
                            <input type="number" class="owlabbulkg-number-input" name="_owlabbulkg[ratio]" value="<?php echo $this->get_config( 'ratio', $this->get_config_default( 'ratio' )); ?>" >
                            <p class="description"><?php _e('While displaying list of galleies at archive page of these galleries, you can set the layout to be grid at theme options, then if you want to make the cover image of this gallery to be another ratio, you can set it here.','owlabbulkg'); ?></p>
                        </td>
                    </tr>

                    <tr id="owlabbulkg-config-slider-grid-sizer-box">
                        <th scope="row">
                            <label for="owlabbulkg-config-grid-sizer"><?php _e( 'Set Cover of this gallery as grid sizer?', 'owlabbulkg' ); ?></label>
                        </th>
                        <td>
                            <?php 
                            $checked = '';
                            if ( $this->get_config( 'grid_sizer', $this->get_config_default( 'grid_sizer' ) ) == 'on' ){
                                $checked = 'checked';
                            } ?>
                            <input type="checkbox" name="_owlabbulkg[grid_sizer]" <?php echo $checked; ?>><?php echo __('Yes, please','owlabbulkg'); ?>
                            <p class="description"><?php _e('Just set one cover image (or none) as grid sizer for your galleries, this will take effect if you set the layout of archive page of bulk gallery at theme options to be as grid and check the same ratio checkbox.','owlabbulkg'); ?></p>
                        </td>

                    </tr>

                </tbody>
            </table>
        </div>
        <?php
    }


    public function get_gallery_layouts(){
        
        $selected_layout = $this->get_config( 'layout', $this->get_config_default( 'layout' ) );

        
        $layouts = array(
            'grid' => __('Grid','owlabbulkg'),
            'horizontal-scroll' => __('Horizontal Scroll','owlabbulkg')
        );

        $output = '';
        foreach ($layouts as $value=>$name){
            
            $selected = '';
            if ($selected_layout == $value){
                $selected = 'selected';
            }
            $output .=  '<option value="'.$value.'" '.$selected.'>'.$name.'</option>';
        }

        echo $output;
       
    }

    public function get_gallery_hovers(){
        
        $selected_hover = $this->get_config( 'hover', $this->get_config_default( 'hover' ) );

        
        $layouts = array(
            __('Simple Icon', "owlabbulkg")     => "simple-icon",
            __('Circle', "owlabbulkg")          => "circle",
            __('Plus light', "owlabbulkg")      => "plus-light",
            __('Plus dark', "owlabbulkg")       => "plus-dark",
            __('Plus colored', "owlabbulkg")    => "plus-color"
        );

        $output = '';
        foreach ($layouts as $name=>$value){
            
            $selected = '';
            if ($selected_hover == $value){
                $selected = 'selected';
            }
            $output .=  '<option value="'.$value.'" '.$selected.'>'.$name.'</option>';
        }

        echo $output;
       
    }
    /**
     * Callback for saving values from owlabbulkg metaboxes.
     *
     * @since 1.0.0
     *
     * @param int $post_id The current post ID.
     * @param object $post The current post object.
     */
    public function save_meta_boxes( $post_id, $post ) {

        // Bail out if we fail a security check.
        if ( ! isset( $_POST['owlabbulkg'] ) || ! wp_verify_nonce( $_POST['owlabbulkg'], 'owlabbulkg' ) || ! isset( $_POST['_owlabbulkg'] ) ) {
            return;
        }

        // Bail out if running an autosave, ajax, cron or revision.
        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return;
        }

        if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
            return;
        }

        if ( defined( 'DOING_CRON' ) && DOING_CRON ) {
            return;
        }

        if ( wp_is_post_revision( $post_id ) ) {
            return;
        }

        // Bail out if the user doesn't have the correct permissions to update the slider.
        if ( ! current_user_can( 'edit_post', $post_id ) ) {
            return;
        }

        

        // Sanitize all user inputs.
        $settings = get_post_meta( $post_id, '_owlabbulkg_slider_data', true );
        if ( empty( $settings ) ) {
            $settings = array();
        }

        // If the ID of the slider is not set or is lost, replace it now.
        if ( empty( $settings['id'] ) || ! $settings['id'] ) {
            $settings['id'] = $post_id;
        }

        // Save the config settings.
        $settings['config']['hover']            = $_POST['_owlabbulkg']['hover'];
        $settings['config']['icon']            = esc_html($_POST['_owlabbulkg']['icon']);
        $settings['config']['layout']           = $_POST['_owlabbulkg']['layout'];
        $settings['config']['same_ratio']       = isset ($_POST['_owlabbulkg']['same_ratio']) ? $_POST['_owlabbulkg']['same_ratio'] : NULL;
        $settings['config']['sidebar']          = isset ($_POST['_owlabbulkg']['sidebar']) ? $_POST['_owlabbulkg']['sidebar'] : NULL;
        $settings['config']['sidebar_content']  = $_POST['_owlabbulkg']['sidebar_content'];
        $settings['config']['lg_col']           = ( absint( $_POST['_owlabbulkg']['lg_col'] ) >0 ) ? absint( $_POST['_owlabbulkg']['lg_col'] ) : $this->get_config_default( 'lg_col' );
        $settings['config']['md_col']           = ( absint( $_POST['_owlabbulkg']['md_col'] ) >0 ) ? absint( $_POST['_owlabbulkg']['md_col'] ) : $this->get_config_default( 'md_col' );
        $settings['config']['sm_col']           = ( absint( $_POST['_owlabbulkg']['sm_col'] ) >0 ) ? absint( $_POST['_owlabbulkg']['sm_col'] ) : $this->get_config_default( 'sm_col' );
        $settings['config']['xs_col']           = ( absint( $_POST['_owlabbulkg']['xs_col'] ) >0 ) ? absint( $_POST['_owlabbulkg']['xs_col'] ) : $this->get_config_default( 'xs_col' );
        $settings['config']['nopadding']        = isset ($_POST['_owlabbulkg']['nopadding']) ? $_POST['_owlabbulkg']['nopadding'] : NULL;
        $settings['config']['ratio']           = ( absint( $_POST['_owlabbulkg']['ratio'] ) >0 ) ? absint( $_POST['_owlabbulkg']['ratio'] ) : $this->get_config_default( 'ratio' );
        $settings['config']['grid_sizer']       = isset ($_POST['_owlabbulkg']['grid_sizer']) ? $_POST['_owlabbulkg']['grid_sizer'] : NULL;
        $settings['config']['short_des']        = $_POST['_owlabbulkg']['short_des'];

        // If on an owlabbulkg post type, map the title and slug of the post object to the custom fields if no value exists yet.
        if ( isset( $post->post_type ) && 'owlabbulkg' == $post->post_type ) {
            if ( empty( $settings['config']['title'] ) ) {
                $settings['config']['title'] = trim( strip_tags( $post->post_title ) );
            }

            if ( empty( $settings['config']['slug'] ) ) {
                $settings['config']['slug'] = sanitize_text_field( $post->post_name );
            }
        }

        // Provide a filter to override settings.
        $settings = apply_filters( 'owlabbulkg_save_settings', $settings, $post_id, $post );

        // Update the post meta.
        update_post_meta( $post_id, '_owlabbulkg_slider_data', $settings );

        // Change states of images in slider from pending to active.
        $this->change_slider_states( $post_id );


        // Fire a hook for addons that need to utilize the cropping feature.
        do_action( 'owlabbulkg_saved_settings', $settings, $post_id, $post );

        // Finally, flush all slider caches to ensure everything is up to date.
        $this->flush_slider_caches( $post_id, $settings['config']['slug'] );

    }

    /**
     * Helper method for retrieving the slider layout for an item in the admin.
     *
     * @since 1.0.0
     *
     * @param int $id The  ID of the item to retrieve.
     * @param array $data  Array of data for the item.
     * @param string $type The type of slide to retrieve.
     * @param int $post_id The current post ID.
     * @return string The  HTML output for the slider item.
     */
    public function get_slider_item( $id, $data, $type, $post_id = 0 ) {


        $item = $this->get_slider_image( $id, $data, $post_id );


        return apply_filters( 'owlabbulkg_slide_item', $item, $id, $data, $type, $post_id );

    }

    /**
     * Helper method for retrieving the slider image layout in the admin.
     *
     * @since 1.0.0
     *
     * @param int $id The  ID of the item to retrieve.
     * @param array $data  Array of data for the item.
     * @param int $post_id The current post ID.
     * @return string The  HTML output for the slider item.
     */
    public function get_slider_image( $id, $data, $post_id = 0 ) {

        $thumbnail = wp_get_attachment_image_src( $id, 'thumbnail' ); ob_start(); ?>
        <li id="<?php echo $id; ?>" class="owlabbulkg-slide owlabbulkg-image owlabbulkg-status-<?php echo $data['status']; ?>" data-owlabbulkg-slide="<?php echo $id; ?>">
            <img src="<?php echo esc_url( $thumbnail[0] ); ?>" alt="<?php esc_attr_e( $data['alt'] ); ?>" />
            <a href="#" class="owlabbulkg-remove-slide" title="<?php esc_attr_e( 'Remove Image Slide from Slider?', 'owlabbulkg' ); ?>"></a>
            <a href="#" class="owlabbulkg-modify-slide" title="<?php esc_attr_e( 'Modify Image Slide', 'owlabbulkg' ); ?>"></a>
            <?php echo $this->get_slider_image_meta( $id, $data, $post_id ); ?>
        </li>
        <?php
        return ob_get_clean();

    }

    /**
     * Helper method for retrieving the slider image metadata.
     *
     * @since 1.0.0
     *
     * @param int $id      The ID of the item to retrieve.
     * @param array $data  Array of data for the item.
     * @param int $post_id The current post ID.
     * @return string      The HTML output for the slider item.
     */
    public function get_slider_image_meta( $id, $data, $post_id ) {

        ob_start();
        ?>
        <div id="owlabbulkg-meta-<?php echo $id; ?>" class="owlabbulkg-meta-container" style="display:none;">
            <div class="media-modal wp-core-ui">
                <a class="media-modal-close" href="#"><span class="media-modal-icon"></span></a>
                <div class="media-modal-content">
                    <div class="media-frame owlabbulkg-media-frame wp-core-ui hide-menu hide-router owlabbulkg-meta-wrap">
                        <div class="media-frame-title">
                            <h1><?php _e( 'Edit Metadata', 'owlabbulkg' ); ?></h1>
                        </div>
                        <div class="media-frame-content">
                            <div class="attachments-browser">
                                <div class="owlabbulkg-meta attachments">
                                    <?php do_action( 'owlabbulkg_before_image_meta_table', $id, $data, $post_id ); ?>
                                    <table id="owlabbulkg-meta-table-<?php echo $id; ?>" class="form-table owlabbulkg-meta-table" data-owlabbulkg-meta-id="<?php echo $id; ?>">
                                        <tbody>

                                            <?php do_action( 'owlabbulkg_before_image_meta_settings', $id, $data, $post_id ); ?>
                                            <tr id="owlabbulkg-title-box-<?php echo $id; ?>" valign="middle">
                                                <th scope="row"><label for="owlabbulkg-title-<?php echo $id; ?>"><?php _e( 'Image Title', 'owlabbulkg' ); ?></label></th>
                                                <td>
                                                    <input id="owlabbulkg-title-<?php echo $id; ?>" class="owlabbulkg-title" type="text" name="_owlabbulkg[meta_title]" value="<?php echo ( ! empty( $data['title'] ) ? esc_attr( $data['title'] ) : '' ); ?>" data-owlabbulkg-meta="title" />
                                                    <p class="description"><?php _e( 'Sets the image title attribute for the image.', 'owlabbulkg' ); ?></p>
                                                </td>
                                            </tr>


                                            <?php do_action( 'owlabbulkg_before_image_meta_alt', $id, $data, $post_id ); ?>
                                            <tr id="owlabbulkg-alt-box-<?php echo $id; ?>" valign="middle">
                                                <th scope="row"><label for="owlabbulkg-alt-<?php echo $id; ?>"><?php _e( 'Image Alt Text', 'owlabbulkg' ); ?></label></th>
                                                <td>
                                                    <input id="owlabbulkg-alt-<?php echo $id; ?>" class="owlabbulkg-alt" type="text" name="_owlabbulkg[meta_alt]" value="<?php echo ( ! empty( $data['alt'] ) ? esc_attr( $data['alt'] ) : '' ); ?>" data-owlabbulkg-meta="alt" />
                                                    <p class="description"><?php _e( 'The image alt text is used for SEO. You should probably fill this one out!', 'owlabbulkg' ); ?></p>
                                                </td>
                                            </tr>

                                            <?php do_action( 'owlabbulkg_before_image_meta_grid_sizer', $id, $data, $post_id ); ?>
                                            <tr id="owlabbulkg-grid_sizer-box-<?php echo $id; ?>" valign="middle">
                                                <th scope="row"><label for="owlabbulkg-grid_sizer-<?php echo $id; ?>"><?php _e( 'Set this image as grid_sizer', 'owlabbulkg' ); ?></label></th>
                                                <td>
                                                    <?php 
                                                    $checked = '';
                                                    if ( !empty ($data['grid_sizer']) ){
                                                        if ($data['grid_sizer'] == 'on'){
                                                            $checked = 'checked';
                                                        }
                                                    }
                                                        
                                                    ?>
                                                    <input id="owlabbulkg-grid_sizer-<?php echo $id; ?>" type="checkbox" name="_owlabbulkg[meta_grid_sizer]" data-owlabbulkg-meta="grid_sizer" <?php echo $checked; ?>/>
                                                    <p class="description"><?php _e( 'Just set one image as grid sizer per gallery, this will take effect if you set the layout of gallery to be as grid and check the same ratio checkbox.', 'owlabbulkg' ); ?></p>
                                                </td>
                                            </tr>

                                            <tr id="owlabbulkg-ratio-box-<?php echo $id; ?>" valign="middle">
                                                <th scope="row"><label for="owlabbulkg-ratio-<?php echo $id; ?>"><?php _e( 'Image Ratio in grid layout', 'owlabbulkg' ); ?></label></th>
                                                <td>
                                                    <input id="owlabbulkg-ratio-<?php echo $id; ?>" class="owlabbulkg-ratio" type="number" name="_owlabbulkg[meta_ratio]" value="<?php echo ( ! empty( $data['ratio'] ) ? esc_attr( $data['ratio'] ) : '' ); ?>" data-owlabbulkg-meta="ratio" />
                                                    <p class="description"><?php _e( 'Sets the ratio of image related to other images in the grid layout.', 'owlabbulkg' ); ?></p>
                                                </td>
                                            </tr>
                                            

                                            

                                        </tbody>
                                    </table>
                                    <?php do_action( 'owlabbulkg_after_image_meta_table', $id, $data, $post_id ); ?>
                                </div><!-- end .owlabbulkg-meta -->
                                
                            </div><!-- end .attachments-browser -->
                        </div><!-- end .media-frame-content -->
                        <div class="media-frame-toolbar">
                            <div class="media-toolbar">
                                <div class="media-toolbar-primary">
                                    <a href="#" class="owlabbulkg-meta-submit button media-button button-large button-primary media-button-insert" title="<?php esc_attr_e( 'Save Metadata', 'owlabbulkg' ); ?>" data-owlabbulkg-item="<?php echo $id; ?>"><?php _e( 'Save Metadata', 'owlabbulkg' ); ?></a>
                                </div><!-- end .media-toolbar-primary -->
                            </div><!-- end .media-toolbar -->
                        </div><!-- end .media-frame-toolbar -->
                    </div><!-- end .media-frame -->
                </div><!-- end .media-modal-content -->
            </div><!-- end .media-modal -->
            <div class="media-modal-backdrop"></div>
        </div>
        <?php
        return ob_get_clean();

    }

    /**
     * Helper method to change a slider state from pending to active. This is done
     * automatically on post save. For previewing sliders before publishing,
     * simply click the "Preview" button and owlabbulkg will load all the images present
     * in the slider at that time.
     *
     * @since 1.0.0
     *
     * @param int $id The current post ID.
     */
    public function change_slider_states( $post_id ) {

        $slider_data = get_post_meta( $post_id, '_owlabbulkg_slider_data', true );
        if ( ! empty( $slider_data['slider'] ) ) {
            foreach ( (array) $slider_data['slider'] as $id => $item ) {
                $slider_data['slider'][$id]['status'] = 'active';
            }
        }

        update_post_meta( $post_id, '_owlabbulkg_slider_data', $slider_data );

    }

    

    /**
     * Helper method to flush slider caches once a slider is updated.
     *
     * @since 1.0.0
     *
     * @param int $post_id The current post ID.
     * @param string $slug The unique slider slug.
     */
    public function flush_slider_caches( $post_id, $slug ) {

        owlabbulkg_Common::get_instance()->flush_slider_caches( $post_id, $slug );

    }

    /**
     * Helper method for retrieving config values.
     *
     * @since 1.0.0
     *
     * @global int $id        The current post ID.
     * @global object $post   The current post object.
     * @param string $key     The config key to retrieve.
     * @param string $default A default value to use.
     * @return string         Key value on success, empty string on failure.
     */
    public function get_config( $key, $default = false ) {

        global $id, $post;

        // Get the current post ID. If ajax, grab it from the $_POST variable.
        if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
            $post_id = absint( $_POST['post_id'] );
        } else {
            $post_id = isset( $post->ID ) ? $post->ID : (int) $id;
        }

        $settings = get_post_meta( $post_id, '_owlabbulkg_slider_data', true );
        if ( isset( $settings['config'][$key] ) ) {
            return $settings['config'][$key];
        } else {
            return $default ? $default : '';
        }

    }

    /**
     * Helper method for setting default config values.
     *
     * @since 1.0.0
     *
     * @param string $key The default config key to retrieve.
     * @return string Key value on success, false on failure.
     */
    public function get_config_default( $key ) {

        $instance = owlabbulkg_Common::get_instance();
        return $instance->get_config_default( $key );

    }



    /**
     * Returns the post types to skip for loading owlabbulkg metaboxes.
     *
     * @since 1.0.0
     *
     * @return array Array of skipped posttypes.
     */
    public function get_skipped_posttypes() {

        $post_types = get_post_types( array( 'public' => true ) );
        unset( $post_types['owlabbulkg'] );
        return apply_filters( 'owlabbulkg_skipped_posttypes', $post_types );

    }


    /**
     * Returns the singleton instance of the class.
     *
     * @since 1.0.0
     *
     * @return object The owlabbulkg_Metaboxes object.
     */
    public static function get_instance() {

        if ( ! isset( self::$instance ) && ! ( self::$instance instanceof Owlabbulkg_Metaboxes ) ) {
            self::$instance = new Owlabbulkg_Metaboxes();
        }

        return self::$instance;

    }

}

// Load the metabox class.
$owlabbulkg_Metaboxes = Owlabbulkg_Metaboxes::get_instance();






