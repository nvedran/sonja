<?php
/**
 * Shortcode class.
 *
 * @since 1.0.0
 *
 * @package owlabkbs
 * @author  owlab
 */
class Owlabbulkg_Grid_Shortcode {


	/**
     * Holds the class object.
     *
     * @since 1.0.0
     *
     * @var object
     */
    public static $instance;

    /**
     * Path to the file.
     *
     * @since 1.0.0
     *
     * @var string
     */
    public $file = __FILE__;

    /**
     * Holds the inline css 
     *
     * @since 1.0.0
     *
     * @var object
     */
    public $inline = '';

     /**
     * Holds the base class object.
     *
     * @since 1.0.0
     *
     * @var object
     */
    public $base;

    /**
     * Holds slider IDs for init firing checks.
     *
     * @since 1.0.0
     *
     * @var array
     */
    public $done = array();

    /**
     * Holds the slider data.
     *
     * @since 1.0.0
     *
     * @var array
     */
    public $data;

    /**
     * Primary class constructor.
     *
     * @since 1.0.0
     */
    public function __construct() {

    	// Load the base class object.
        $this->base = Owlabbulkg::get_instance();

        // Register main slider style.
        // this slider will use the same js file that is used by blog slider se we just need to enque it - rslider at functions-styles-scripts.php

        // Register main slider script.
        // also rslider css is needed which is registered with the toranj theme functions-styles-scripts.php

        // Load hooks and filters.
        add_shortcode( 'bulkgal_grid', array( $this, 'shortcode' ) );
        // Use shortcodes in text widgets.
        add_filter( 'widget_text', 'do_shortcode' );

    }

    /**
     * Creates the shortcode for the plugin.
     *
     * @since 1.0.0
     *
     * @global object $post The current post object.
     *
     * @param array $atts Array of shortcode attributes.
     * @return string     The slider output.
     */
    public function shortcode( $atts ) {

        global $post;

        $myatts = shortcode_atts(array(
			'galleryid'                     =>'',
			'slug' 		                    =>'',
			'remove_spaces_between_images'	=> '',
			'lg_cols'		=> 4,
			'md_cols'	    => 3,
			'sm_cols'		=> 2,
			'xs_cols'		=> 2,
            'overlay_type'  => 'plus-light'
		), $atts,'bulkgal_grid');

        extract($myatts);


        // If no attributes have been passed, we just echo a message
        $gallery_id = false;
        if ( empty( $atts ) ) 
        {
            _e('Hey from Bulk gallery shortcode! You need to pass id parameter to me!','owlabbulkg');
            return;
        } 
        else if ( isset( $atts['galleryid'] ) ) 
        {
            $gallery_id = (int) $atts['galleryid'];
            $data      = is_preview() ? $this->base->_get_gallery( $gallery_id ) : $this->base->get_gallery( $gallery_id );
        } 
        else if ( isset( $atts['slug'] ) ) 
        {
            $gallery_id = $atts['slug'];
            $data      = is_preview() ? $this->base->_get_gallery_by_slug( $gallery_id ) : $this->base->get_gallery_by_slug( $gallery_id );
        } else {
            // A custom attribute must have been passed. Allow it to be filtered to grab data from a custom source.
            $data = apply_filters( 'owlabbulkg_custom_gallery_data', false, $atts, $post );
        }

        // If there is no data and the attribute used is an ID, try slug as well.
        if ( ! $data && isset( $atts['galleryid'] ) ) {
            $gallery_id = (int) $atts['galleryid'];
            $data      = is_preview() ? $this->base->_get_gallery_by_slug( $gallery_id ) : $this->base->get_gallery_by_slug( $gallery_id );
        }

        // If there is no data to output or the slider is inactive, do nothing.
        if ( ! $data ) {
            return false;
        }

        //add atts to the $data
        $data = $data + $myatts;
        $this->data[$data['id']] = $data;


        //outputing the layout
        $no_padding_class = $remove_spaces_between_images=='yes' ? ' no-padding' : '';

        $output = '
        <!-- Gallery wrapper --> 
        <div class="grid-portfolio tj-lightbox-gallery'.$no_padding_class.'" lg-cols="'.$lg_cols.'" md-cols="'.$md_cols.'" sm-cols="'.$sm_cols.'" xs-cols="'.$xs_cols.'">
        ';
        
        $item_overlay = owlab_get_gallery_overlay($overlay_type,'fa-link');

        foreach ($data['slider'] as $id=>$image) {
            
            if( isset($image['src']) )
            {
            	
            	$img = wp_get_attachment_image_src( $id,'blog-thumb' );
            	$img_src =$img[0];
            	
                $output .='
                <!-- Gallery Item -->       
                <div class="gp-item '.$item_overlay['parent_class'].'"> 
                    <a href="'.$image['src'].'"  class="lightbox-gallery-item" title="'.$image['title'].'">
                        '.owlab_lazy_image( $img, $image['alt'],false ).'
                        <!-- Item Overlay -->   
                        '.$item_overlay['markup'].'
                        <!-- /Item Overlay -->  
                    </a>
                </div>
                <!-- /Gallery Item -->';
           		
            }
        }
        
        $output .= '</div>';
        
        return $output;


    }



	/**
     * Returns the singleton instance of the class.
     *
     * @since 1.0.0
     *
     * @return object The Owlabbulkg_Grid_Shortcode object.
     */
    public static function get_instance() {

        if ( ! isset( self::$instance ) && ! ( self::$instance instanceof Owlabbulkg_Grid_Shortcode ) ) {
            self::$instance = new Owlabbulkg_Grid_Shortcode();
        }

        return self::$instance;

    }
}

// Load the shortcode class.
$owlabbulkg_Grid_Shortcode = Owlabbulkg_Grid_Shortcode::get_instance();