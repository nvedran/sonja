$(document).ready(function(){

	$('#home-intro').height($(window).height()-97);

		/* Navigation
		----------------------------------------------*/
		var navigation={
			init:function(){
				this.sidenav=$('#side-nav');
				this.rootLi=this.sidenav.children('li');
				this.sections=$('section');
				this.activeSection=this.sections.filter('.active').first();
				this.sidebar=$('#sidebar');
				this.sideDir='out';
				this.intro=true;
				this.bindUIActions();
			},
			bindUIActions:function(){
				var self=this;

				self.rootLi.on('click',function(e){
					e.preventDefault();
					var $this=$(this);

					if ($this.hasClass('active')){
						return false;
					}
					
					//Detect previous active menu and sub-menu
					var previousActive=self.rootLi.filter('.active'),
						previousSub=previousActive.find('.sub-menu');

					//Handle current active menu
					self.rootLi.removeClass('active');
					$this.addClass('active');

					//Handle sub-menu
					var $submenu=$this.find('.sub-menu');
					self.animateSubs(previousSub,$submenu);

					
					//Animate target section
					var $target=self.sections.filter($this.find('a').first().attr('href'));
					self.animateSections(self.activeSection,$target);


				})

				$('#intro-button').on('click',function(e){
					e.preventDefault();
					self.rootLi.eq(0).trigger('click');
				});

				$('.sub-menu li').on('click',function(e){
					e.preventDefault();

					var $this=$(this),
						target=self.activeSection.find('.sub-section-title').eq($this.index());
					
					TweenMax.to($('body'),0.3,{scrollTop:target.offset().top-150});
					
				});


				$('#toggle-sidebar').on('click',function(e){
					e.preventDefault();
					self.toggleSide(self.sideDir,$(this));

				});
			},
			animateSections:function(source,target){
				var self=this;

				source.hide();
				target.show();
				$('body').scrollTop(0);
				self.activeSection=target;

				if (self.intro){
					self.handleIntro();
				}
			},
			animateSubs:function(source,target){
				var self=this;

				if (source){
					source.slideUp();
				}

				if(target){
					target.slideDown();
				}
			},
			toggleSide:function(dir,elem){
				var self=this;
				if (self.inAnimation){
					return false
				}
				self.inAnimation=true;

				var animOut=new TimelineLite({paused:true});
				var animIn=new TimelineLite({paused:true});

				animOut
				.to(self.sidebar,1,{left:-350,ease:Power4.easeOut})
				.to(elem,0.1,{right:-50,ease:Power4.easeOut,onComplete:function(){
					elem.find('i').removeClass('fa-long-arrow-left').addClass('fa-long-arrow-right');
					self.inAnimation=false;
					self.sideDir='in';
				}},'-=0.7');

				animIn
				.to(self.sidebar,1,{left:0,ease:Power4.easeOut})
				.to(elem,0.1,{right:0,ease:Power4.easeOut,onComplete:function(){
					elem.find('i').removeClass('fa-long-arrow-right').addClass('fa-long-arrow-left');
					self.inAnimation=false;
					self.sideDir='out';
				}});

				if (dir=='out'){
					animOut.play();
				}else{
					animIn.play();
				}
			},
			handleIntro:function(){
				var self=this;
				var height=$('#home-intro').height();
				TweenMax.to($('body'),1,{scrollTop:height,ease:Power4.easeOut,onComplete:function(){
					$('#home-intro').hide();
					$('body').scrollTop(0);
					self.intro=false;
				}});
				
			}
			

		}
		navigation.init();

		/* light box
		----------------------------------------------*/
		var lightBox={

			init:function(){
				var self=this;

				self.localvideo={
					autoPlay:false,
					preload:'metadata',
					webm :true,
					ogv:false	
				}

				this.bindUIActions();
			
				
			},
			generateVideo:function(src,poster){
				var self=this;
				//here we generate video markup for html5 local video
				//We assumed that you have mp4 and webm or ogv format in samepath (video/01/01.mp4 & video/01/01.webm)
				var basePath=src.substr(0, src.lastIndexOf('.mp4'));
				var headOptions='';
				if (self.localvideo.autoPlay){
					headOptions+=' autoplay';
				}
				headOptions +='preload="'+self.localvideo.preload+'"';

				var markup='<video class="mejs-player popup-mejs video-html5" controls '+headOptions+' poster="'+poster+'">'+
					'<source src="'+src+'" type="video/mp4" />';

				if (self.localvideo.webm){
					markup+='<source src="'+basePath+'.webm" type="video/webm" />'
				}
		
				if (self.localvideo.ogv){
					markup+='<source src="'+basePath+'.ogv" type="video/ogg" />'
				}
				markup+='</video>'+'<div class="mfp-close"></div>';

				return markup;

			},
			bindUIActions:function(){
				var self=this,
					$body=$('body');

				self.singleBox($('.tj-lightbox'));

				$('.tj-lightbox-gallery').each(function(){
					self.galleyBox($(this));	
				});
				
				$('body').on('click','.mfp-container',function(e){
				if( e.target !== this ) 
       				return;
				$(this).find('.mfp-close').trigger('click');
			});


			},
			singleBox:function($elem){
				var self=this;
				$elem.magnificPopup({
					type: 'image',
					closeOnContentClick: false,
					closeOnBgClick:false,
					mainClass: 'mfp-fade',
					 iframe: {
						markup: '<div class="mfp-iframe-scaler">'+
					            '<div class="mfp-close"></div>'+
					            '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>'+
					            '<div class="mfp-title"></div>'+
					          '</div>'
					},
					callbacks:{
						elementParse: function(item) {
							var popType=item.el.attr('data-type')||'image';
							if (popType=='localvideo'){
								item.type='inline';
								var poster=item.el.attr('data-poster')||'';
								item.src=self.generateVideo(item.src,poster);
							}else{
								item.type=popType;
							}
					    },
			    		markupParse: function(template, values, item) {
					    	values.title = item.el.attr('title');
					    },
					    open: function() {
					    	sideS.$exteras=$('.move-with-js').add('.mfp-wrap');
					  		$('.popup-mejs').mediaelementplayer();
					  	}
			    	},
					image: {
						verticalFit: true
					}
				});

			},
			galleyBox:function($elem){
				var self=this,
					$this=$elem,
					itemsArray=[];


					
					$elem.magnificPopup({
						delegate: '.lightbox-gallery-item',
					    closeOnBgClick:false,
					    closeOnContentClick:false,
					    removalDelay: 300,
					    mainClass: 'mfp-fade',
					    iframe: {
							markup: '<div class="mfp-iframe-scaler">'+
						            '<div class="mfp-close"></div>'+
						            '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>'+
						            '<div class="mfp-title"></div>'+
						            '<div class="mfp-counter"></div>'+
						          '</div>'
						},
					    gallery: {
					      enabled: true,
					       tPrev: 'Previous',
						   tNext: 'Next',
						   tCounter: '%curr% / %total%',
						   arrowMarkup: '<a class="tj-mp-action tj-mp-arrow-%dir% mfp-prevent-close" title="%title%"><i class="fa fa-angle-%dir%"></i></a>',
					    },
					    callbacks:{
					    	elementParse:function(item){
								
								var	popType=item.el.attr('data-type') || 'image',
									source=item.el.attr('href');
								

								if (popType=='localvideo'){
									item.src=self.generateVideo(source,item.el.attr('data-poster')||'');
									item.type='inline';
								}else{
									item.type=popType;
								}

					    	},
					    	open:function(){
								sideS.$exteras=$('.move-with-js').add('.mfp-wrap');
								$('.popup-mejs').mediaelementplayer();
					    	},
					    	change: function() {
						        if (this.isOpen) {
						            this.wrap.addClass('mfp-open');
						        }
						       $('.popup-mejs').mediaelementplayer();
						    }
					    },
					    type: 'image' // this is a default type
					});

					itemsArray=[];
			}
		}
		lightBox.init();
});