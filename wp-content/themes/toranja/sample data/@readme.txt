Important notice
============================
We recommend you to use the toranj-sample-data-all__with_images.xml file to import sample data.
Using that file there might be some notifications that wordpress could not import certain pages or files.
That notice is ok and there is no Error there.
You can continue your work after import.

why I get those "Could not import..." ?
=============================
Because we have included Woocommerce sample data inside it.
In case you haven't installed WooCommerce plugin, the importer won't import those data and 
will notify you that could not import those pages or files.
It is normal.
