<?php
/**
 * Include and setup custom metaboxes and fields.
 *
 * @category YourThemeOrPlugin
 * @package  Metaboxes
 * @license  http://www.opensource.org/licenses/gpl-license.php GPL v2.0 (or later)
 * @link     https://github.com/jaredatch/Custom-Metaboxes-and-Fields-for-WordPress
 */

add_filter( 'cmb_meta_boxes', 'cmb_sample_metaboxes' );
/**
 * Define the metabox and field configurations.
 *
 * @param  array $meta_boxes
 * @return array
 */
function cmb_sample_metaboxes( array $meta_boxes ) {

	// Start with an underscore to hide fields from custom fields list
	$prefix = '_portfolio_';

	$meta_boxes[] = array(
		'id'         => 'portfolio_metabox',
		'title'      => 'Portfolio Metabox',
		'pages'      => array( 'portfolio', ), // Post type
		'context'    => 'normal',
		'priority'   => 'high',
		'show_names' => true, // Show field names on the left
		'fields'     => array(
			array( // Text Input
			    'name' => 'First project image', // <label>
			    'desc'  => 'Add the url to the first image, mandatory', // description
			    'id'    => $prefix . 'image1', // field id and name
			    'type'  => 'file' // type of field
			    ),
			array(
				'name' => 'Second project image', // <label>
			    'desc'  => 'Add the url to the second image, if used', // description
			    'id'    => $prefix . 'image2', // field id and name
			    'type'  => 'file' // type of field
			    ),
			array(
				'name' => 'Third project image', // <label>
			    'desc'  => 'Add the url to the third image, if used', // description
			    'id'    => $prefix . 'image3', // field id and name
			    'type'  => 'file' // type of field
			    ),
			array(
				'name' => 'Fourth project image', // <label>
			    'desc'  => 'Add the url to the fourth image, if used', // description
			    'id'    => $prefix . 'image4', // field id and name
			    'type'  => 'file' // type of field
			    ),
			array(
				'name' => 'Fifth project image', // <label>
			    'desc'  => 'Add the url to the fifth image, if used', // description
			    'id'    => $prefix . 'image5', // field id and name
			    'type'  => 'file' // type of field
			    ),
			array( // Text Input
			    'name' => 'Thumbnail', // <label>
			    'desc'  => 'Add the url to the thumbnail that will show up initially', // description
			    'id'    => $prefix . 'thumb', // field id and name
			    'type'  => 'file' // type of field
			    ),
			array( // Text Input
			    'name' => 'Description', // <label>
			    'desc'  => 'Some description for your project.', // description
			    'id'    => $prefix . 'description', // field id and name
			    'type'    => 'wysiwyg',
				'options' => array(	'textarea_rows' => 5, ),
			    ),
			array( // Text Input
			    'name' => 'Video URL', // <label>
			    'desc'  => 'The video will open on the "zoom" icon. Only used if applicable.', // description
			    'id'    => $prefix . 'video', // field id and name
			    'type'  => 'text' // type of field
			    ),
			array( // Text Input
			    'name' => 'Button text', // <label>
			    'desc'  => 'The text on the call to action button, if applicable.', // description
			    'id'    => $prefix . 'buttontext', // field id and name
			    'type'  => 'text' // type of field
			    ),
			array( // Text Input
			    'name' => 'Button URL', // <label>
			    'desc'  => 'The url on the call to action button, if applicable.', // description
			    'id'    => $prefix . 'buttonurl', // field id and name
			    'type'  => 'text' // type of field
			    )
		),
	);

	$prefix = '_separator_';

	$meta_boxes[] = array(
		'id'         => 'separator_metabox',
		'title'      => 'Parallax separator image and texts',
		'pages'      => array( 'page', ), // Post type
		'context'    => 'normal',
		'priority'   => 'high',
		'show_names' => true, // Show field names on the left
		'fields' => array(
			array( // Text Input
			    'name' => 'Separator background image', // <label>
			    'desc'  => 'The parallax background image that shows up below the page content ,', // description
			    'id'    => $prefix . 'bgimage', // field id and name
			    'type'  => 'file' // type of field
			    ),
			array( // Text Input
			    'name' => 'Quote 1 text', // <label>
			    'desc'  => 'Text to be used on the background image. This one is mandatory if you set a background image.', // description
			    'id'    => $prefix . 'quote1', // field id and name
			    'type'  => 'text', // type of field
			    ),
			array( // Text Input
			    'name' => 'Quote 1 author', // <label>
			    'desc'  => 'If you want to set the author of the above quote or some extra details, add it here. Optional.', // description
			    'id'    => $prefix . 'quote1_author', // field id and name
			    'type'  => 'text', // type of field
			    ),
			array( // Text Input
			    'name' => 'Quote 2 text', // <label>
			    'desc'  => 'If you set this, the text on the separator will be a slider. It\'s optional.', // description
			    'id'    => $prefix . 'quote2', // field id and name
			    'type'  => 'text', // type of field
			    ),
			array( // Text Input
			    'name' => 'Quote 2 author', // <label>
			    'desc'  => 'If you want to set the author of the above quote or some extra details, add it here. Optional.', // description
			    'id'    => $prefix . 'quote2_author', // field id and name
			    'type'  => 'text', // type of field
			    ),
			array( // Text Input
			    'name' => 'Quote 3 text', // <label>
			    'desc'  => 'If you set this, the text on the separator will be a slider. It\'s optional.', // description
			    'id'    => $prefix . 'quote3', // field id and name
			    'type'  => 'text', // type of field
			    ),
			array( // Text Input
			    'name' => 'Quote 3 author', // <label>
			    'desc'  => 'If you want to set the author of the above quote or some extra details, add it here. Optional.', // description
			    'id'    => $prefix . 'quote3_author', // field id and name
			    'type'  => 'text', // type of field
			    ),
			array( // Text Input
			    'name' => 'Quote 4 text', // <label>
			    'desc'  => 'If you set this, the text on the separator will be a slider. It\'s optional.', // description
			    'id'    => $prefix . 'quote4', // field id and name
			    'type'  => 'text', // type of field
			    ),
			array( // Text Input
			    'name' => 'Quote 4 author', // <label>
			    'desc'  => 'If you want to set the author of the above quote or some extra details, add it here. Optional.', // description
			    'id'    => $prefix . 'quote4_author', // field id and name
			    'type'  => 'text', // type of field
			    ),
		)
	);

	$prefix = '_page_';

	$meta_boxes[] = array(
		'id'         => 'page_metabox',
		'title'      => 'Page extra details',
		'pages'      => array( 'page', ), // Post type
		'context'    => 'normal',
		'priority'   => 'high',
		'show_names' => true, // Show field names on the left
		'fields' => array(
			array( // Text Input
			    'name' => 'Title on the homepage', // <label>
			    'desc'  => 'If you want to set a custom title for that page on the homepage, you can set it here. If not, the page title will be used.', // description
			    'id'    => $prefix . 'title', // field id and name
			    'type'  => 'text', // type of field
			    ),
			array( // Text Input
			    'name' => 'Description', // <label>
			    'desc'  => 'Some small description for your page, it will show up on the homepage below the title', // description
			    'id'    => $prefix . 'description', // field id and name
			    'type'  => 'text', // type of field
			    ),
			array( // Text Input
			    'name' => 'Background image', // <label>
			    'desc'  => 'If you want to use a custom background image for the page on the homepage, add it here ,', // description
			    'id'    => $prefix . 'bgimage', // field id and name
			    'type'  => 'file' // type of field
			    ),
			array( // Text Input
			    'name' => 'Background color', // <label>
			    'desc'  => 'If you want a static color and not a background image, you can put the hex code here, e.g. #000000 <a href="http://www.colorpicker.com/">http://www.colorpicker.com/</a>', // description
			    'id'    => $prefix . 'bgcolor', // field id and name
			    'type'  => 'colorpicker' // type of field
			    ),
			array( // Text Input
			    'name' => 'Text color', // <label>
			    'desc'  => 'If you set a custom background image or color, you may want to change the color of the text as well, e.g. #FFFFFF <a href="http://www.colorpicker.com/">http://www.colorpicker.com/</a>.', // description
			    'id'    => $prefix . 'color', // field id and name
			    'type'  => 'colorpicker' // type of field
			    ),
			array( // Select box
			    'name' => 'Layout type', 
			    'desc'  => 'Will you use the built-in shortcodes? If the answer is yes, you\'ll need to use the column shortcodes, e.g. [full] or one_third, one_fourth, etc. If no shortcodes are used, you can choose the default layout',
			    'id'    => $prefix . 'type', 
			    'type'  => 'select',
			    'options' => array ( 
			        'one' => array ( 
			            'name' => 'With shortcodes - will use the column shortcodes(you can check the docs for them)', 
			            'value' => '1' 
			        ),
			        'two' => array (
			            'name' => 'Without shortcodes(the theme will add the column shortcode automatically)',
			            'value' => '2'
			        ),
			    ),
			    'std' => '2'
			),
		)
	);

	// Add other metaboxes as needed

	return $meta_boxes;
}

add_action( 'init', 'cmb_initialize_cmb_meta_boxes', 9999 );
/**
 * Initialize the metabox class.
 */
function cmb_initialize_cmb_meta_boxes() {

	if ( ! class_exists( 'cmb_Meta_Box' ) )
		require_once 'init.php';

}