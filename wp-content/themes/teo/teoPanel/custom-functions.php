<?php 
add_filter('wp_title', 'teo_filter_wp_title', 9, 3);
function teo_filter_wp_title( $old_title, $sep, $sep_location ) {
	$ssep = ' ' . $sep . ' ';
	if (is_home() ) {
		return get_bloginfo('name');
	}
	elseif(is_single() || is_page() )
	{
		return get_the_title();
	}
	elseif( is_category() ) $output = $ssep . 'Category';
	elseif( is_tag() ) $output = $ssep . 'Tag';
	elseif( is_author() ) $output = $ssep . 'Author';
	elseif( is_year() || is_month() || is_day() ) $output = $ssep . 'Archives';
	else $output = NULL;
	 
	// get the page number we're on (index)
	if( get_query_var( 'paged' ) )
	$num = $ssep . 'page ' . get_query_var( 'paged' );
	 
	// get the page number we're on (multipage post)
	elseif( get_query_var( 'page' ) )
	$num = $ssep . 'page ' . get_query_var( 'page' );
		 
	// else
	else $num = NULL;
		 
	// concoct and return new title
	return get_bloginfo( 'name' ) . $output . $old_title . $num;
}

//This function shows the top menu if the user didn't create the menu in Appearance -> Menus.
if( !function_exists( 'show_top_menu') )
{
	function show_top_menu() {
		global $teo_options;
		echo '<ul class="nav">';
		if(isset($teo_options['pages_topmenu']) && $teo_options['pages_topmenu'] != '' )
			$pages = get_pages( array('include' => $teo_options['pages_topmenu'], 'sort_column' => 'menu_order', 'sort_order' => 'ASC') );
		else
			$pages = get_pages('number=4&sort_column=menu_order&sort_order=ASC');
		$count = count($pages);
		if($teo_options['menu_homelink'] == '1') 
			echo '<li><a class="active" data-target="#home" href="' . get_home_url() . '/#home">Home</a>';
		for($i = 0; $i < $count; $i++)
		{
			echo '<li><a data-target="#' . $pages[$i]->post_name . '" href="' . get_home_url() . '/#' . $pages[$i]->post_name . '">' . $pages[$i]->post_title . '</a></li>' . PHP_EOL;
		}
		if(isset($teo_options['blog_page']) && $teo_options['blog_page'] != '')
			echo '<li><a href="' . get_permalink($teo_options['blog_page'][0]) . '">Blog</a></li>';
		echo '<li><a data-target="#contact" href="' . get_home_url() . '/#contact">Contact</a></li>';
		echo '</ul>';
	}
}

add_action('wp_head', 'teo_customization');
//This function handles the Colorization tab of the theme options
if(! function_exists('teo_customization'))
{
	function teo_customization() {
		//favicon
		global $teo_options;

		//Loading the google web fonts on the page.
		$loaded[] = 'Oswald';
		$loaded[] = 'Open+Sans';
		if(!in_array($teo_options['body_font'], $loaded))
		{
			echo '<link id="' . $teo_options['body_font'] . '" href="http://fonts.googleapis.com/css?family=' . $teo_options['body_font'] . '" rel="stylesheet" type="text/css" />' . PHP_EOL;
			$loaded[] = $teo_options['body_font'];
		}

		if(isset($teo_options['top_headertext_font']) && !in_array($teo_options['top_headertext_font'], $loaded))
		{
			echo '<link id="' . $teo_options['top_headertext_font'] . '" href="http://fonts.googleapis.com/css?family=' . $teo_options['top_headertext_font'] . '" rel="stylesheet" type="text/css" />' . PHP_EOL;
			$loaded[] = $teo_options['top_headertext_font'];
		}

		if(isset($teo_options['top_smalltext_font']) && !in_array($teo_options['top_smalltext_font'], $loaded))
		{
			echo '<link id="' . $teo_options['top_smalltext_font'] . '" href="http://fonts.googleapis.com/css?family=' . $teo_options['top_smalltext_font'] . '" rel="stylesheet" type="text/css" />' . PHP_EOL;
			$loaded[] = $teo_options['top_smalltext_font'];
		}

		if(isset($teo_options['top_button_font']) && !in_array($teo_options['top_button_font'], $loaded))
		{
			echo '<link id="' . $teo_options['top_button_font'] . '" href="http://fonts.googleapis.com/css?family=' . $teo_options['top_button_font'] . '" rel="stylesheet" type="text/css" />' . PHP_EOL;
			$loaded[] = $teo_options['top_button_font'];
		}

		if(isset($teo_options['nav_font']) && !in_array($teo_options['nav_font'], $loaded))
		{
			echo '<link id="' . $teo_options['nav_font'] . '" href="http://fonts.googleapis.com/css?family=' . $teo_options['nav_font'] . '" rel="stylesheet" type="text/css" />' . PHP_EOL;	
			$loaded[] = $teo_options['nav_font'];
		}

		if(isset($teo_options['pagetitle_font']) && !in_array($teo_options['pagetitle_font'], $loaded))
		{
			echo '<link id="' . $teo_options['pagetitle_font'] . '" href="http://fonts.googleapis.com/css?family=' . $teo_options['pagetitle_font'] . '" rel="stylesheet" type="text/css" />' . PHP_EOL;
			$loaded[] = $teo_options['pagetitle_font'];
		}	
		if(isset($teo_options['subheader_font']) && !in_array($teo_options['subheader_font'], $loaded))
		{
			echo '<link id="' . $teo_options['subheader_font'] . '" href="http://fonts.googleapis.com/css?family=' . $teo_options['subheader_font'] . '" rel="stylesheet" type="text/css" />' . PHP_EOL;
			$loaded[] = $teo_options['subheader_font'];
		}	
		if(isset($teo_options['h3_font']) && !in_array($teo_options['h3_font'], $loaded))
		{
			echo '<link id="' . $teo_options['h3_font'] . '" href="http://fonts.googleapis.com/css?family=' . $teo_options['h3_font'] . '" rel="stylesheet" type="text/css" />' . PHP_EOL;
			$loaded[] = $teo_options['h3_font'];
		}
		if(isset($teo_options['h4_font']) && !in_array($teo_options['h4_font'], $loaded))
		{
			echo '<link id="' . $teo_options['h4_font'] . '" href="http://fonts.googleapis.com/css?family=' . $teo_options['h4_font'] . '" rel="stylesheet" type="text/css" />' . PHP_EOL;
			$loaded[] = $teo_options['h4_font'];
		}
		if(isset($teo_options['separator_font']) && !in_array($teo_options['separator_font'], $loaded))
		{
			echo '<link id="' . $teo_options['separator_font'] . '" href="http://fonts.googleapis.com/css?family=' . $teo_options['separator_font'] . '" rel="stylesheet" type="text/css" />' . PHP_EOL;
			$loaded[] = $teo_options['separator_font'];
		}
		if(isset($teo_options['footer_font']) && !in_array($teo_options['footer_font'], $loaded))
		{
			echo '<link id="' . $teo_options['footer_font'] . '" href="http://fonts.googleapis.com/css?family=' . $teo_options['footer_font'] . '" rel="stylesheet" type="text/css" />' . PHP_EOL;
			$loaded[] = $teo_options['footer_font'];
		}

		if(isset($teo_options['favicon']) && $teo_options['favicon'] != '')
			echo '<link rel="shortcut icon" href="' . $teo_options['favicon'] . '" />';
		//add custom CSS as per the theme options only if custom colorization was enabled.
		if(isset($teo_options['enable_colorization']) && $teo_options['enable_colorization'] == 1)
		{
			$loaded = array();
			echo "\n<style type='text/css'> \n";
			echo '
			p, body, .section-default p, .section-default { font-size: ' . $teo_options['body_size'] . 'px; color: ' . $teo_options['body_color_white'] . '; font-family: \'' . str_replace('+', ' ', $teo_options['body_font']) . '\',sans-serif; }
			.homepage-slider .text-section .title { font-size: ' . $teo_options['top_headertext_size'] . 'px; color: ' . $teo_options['top_headertext_color'] . '; font-family: \'' . str_replace('+', ' ', $teo_options['top_headertext_font']) . '\',sans-serif; }
			.homepage-slider .text-section .sub-title { font-size: ' . $teo_options['top_smalltext_size'] . 'px; color: ' . $teo_options['top_smalltext_color'] . '; font-family: \'' . str_replace('+', ' ', $teo_options['top_smalltext_font']) . '\',sans-serif; }
			.homepage-slider .text-section .homepage-slider-button { font-size: ' . $teo_options['top_button_size'] . 'px; color: ' . $teo_options['top_button_color'] . '; font-family: \'' . str_replace('+', ' ', $teo_options['top_button_font']) . '\',sans-serif; }
			#mainNav ul li a { font-size: ' . $teo_options['nav_size'] . 'px; color: ' . $teo_options['nav_color'] . ' !important; font-family: \'' . str_replace('+', ' ', $teo_options['nav_font']) . '\',sans-serif; }
			#mainNav ul li.active a { color: ' . $teo_options['nav_hovercolor'] . ' !important; }
			.section-title h1 { font-size: ' . $teo_options['pagetitle_size'] . 'px; color: ' . $teo_options['pagetitle_color'] . '; font-family: \'' . str_replace('+', ' ', $teo_options['pagetitle_font']) . '\',sans-serif; }
			.section-title h2 { font-size: ' . $teo_options['subheader_size'] . 'px; color: ' . $teo_options['subheader_color'] . '; font-family: \'' . str_replace('+', ' ', $teo_options['subheader_font']) . '\',sans-serif; }
			h3 { color: ' . $teo_options['h3_color'] . '; font-size: ' . $teo_options['h3_size'] . 'px; font-family: \'' . str_replace('+', ' ', $teo_options['h3_font']) . '\',sans-serif; }
			h4 { color: ' . $teo_options['h4_color'] . '; font-size: ' . $teo_options['h4_size'] . 'px; font-family: \'' . str_replace('+', ' ', $teo_options['h4_font']) . '\',sans-serif; }
			.quote, .quote-author { color: ' . $teo_options['separator_color'] . ' !important; font-family: \'' . str_replace('+', ' ', $teo_options['separator_font']) . '\',sans-serif !important; }
			.quote { font-size: ' . $teo_options['separator_size'] . 'px !important; }
			.quote-author { font-size: ' . $teo_options['separator_authorsize'] . 'px !important; }
			footer p, footer a { font-size: ' . $teo_options['footer_size'] . 'px; color: ' . $teo_options['footer_color'] . '; font-family: \'' . str_replace('+', ' ', $teo_options['footer_font']) . '\',sans-serif; }
			';
			echo '</style>';
		}
	}
}

/**
 * Title         : Aqua Resizer
 * Description   : Resizes WordPress images on the fly
 * Version       : 1.1.7
 * Author        : Syamil MJ
 * Author URI    : http://aquagraphite.com
 * License       : WTFPL - http://sam.zoy.org/wtfpl/
 * Documentation : https://github.com/sy4mil/Aqua-Resizer/
 *
 * @param string  $url    - (required) must be uploaded using wp media uploader
 * @param int     $width  - (required)
 * @param int     $height - (optional)
 * @param bool    $crop   - (optional) default to soft crop
 * @param bool    $single - (optional) returns an array if false
 * @uses  wp_upload_dir()
 * @uses  image_resize_dimensions() | image_resize()
 * @uses  wp_get_image_editor()
 *
 * @return str|array
 */

function aq_resize( $url, $width = null, $height = null, $crop = null, $single = true, $upscale = false ) {

	// Validate inputs.
	if ( ! $url || ( ! $width && ! $height ) ) return false;

	// Caipt'n, ready to hook.
	if ( true === $upscale ) add_filter( 'image_resize_dimensions', 'aq_upscale', 10, 6 );

	// Define upload path & dir.
	$upload_info = wp_upload_dir();
	$upload_dir = $upload_info['basedir'];
	$upload_url = $upload_info['baseurl'];
	
	$http_prefix = "http://";
	$https_prefix = "https://";
	
	/* if the $url scheme differs from $upload_url scheme, make them match 
	   if the schemes differe, images don't show up. */
	if(!strncmp($url,$https_prefix,strlen($https_prefix))){ //if url begins with https:// make $upload_url begin with https:// as well
		$upload_url = str_replace($http_prefix,$https_prefix,$upload_url);
	}
	elseif(!strncmp($url,$http_prefix,strlen($http_prefix))){ //if url begins with http:// make $upload_url begin with http:// as well
		$upload_url = str_replace($https_prefix,$http_prefix,$upload_url);		
	}
	

	// Check if $img_url is local.
	if ( false === strpos( $url, $upload_url ) ) return false;

	// Define path of image.
	$rel_path = str_replace( $upload_url, '', $url );
	$img_path = $upload_dir . $rel_path;

	// Check if img path exists, and is an image indeed.
	if ( ! file_exists( $img_path ) or ! getimagesize( $img_path ) ) return false;

	// Get image info.
	$info = pathinfo( $img_path );
	$ext = $info['extension'];
	list( $orig_w, $orig_h ) = getimagesize( $img_path );

	// Get image size after cropping.
	$dims = image_resize_dimensions( $orig_w, $orig_h, $width, $height, $crop );
	$dst_w = $dims[4];
	$dst_h = $dims[5];

	// Return the original image only if it exactly fits the needed measures.
	if ( ! $dims && ( ( ( null === $height && $orig_w == $width ) xor ( null === $width && $orig_h == $height ) ) xor ( $height == $orig_h && $width == $orig_w ) ) ) {
		$img_url = $url;
		$dst_w = $orig_w;
		$dst_h = $orig_h;
	} else {
		// Use this to check if cropped image already exists, so we can return that instead.
		$suffix = "{$dst_w}x{$dst_h}";
		$dst_rel_path = str_replace( '.' . $ext, '', $rel_path );
		$destfilename = "{$upload_dir}{$dst_rel_path}-{$suffix}.{$ext}";

		if ( ! $dims || ( true == $crop && false == $upscale && ( $dst_w < $width || $dst_h < $height ) ) ) {
			// Can't resize, so return false saying that the action to do could not be processed as planned.
			return false;
		}
		// Else check if cache exists.
		elseif ( file_exists( $destfilename ) && getimagesize( $destfilename ) ) {
			$img_url = "{$upload_url}{$dst_rel_path}-{$suffix}.{$ext}";
		}
		// Else, we resize the image and return the new resized image url.
		else {

			// Note: This pre-3.5 fallback check will edited out in subsequent version.
			if ( function_exists( 'wp_get_image_editor' ) ) {

				$editor = wp_get_image_editor( $img_path );

				if ( is_wp_error( $editor ) || is_wp_error( $editor->resize( $width, $height, $crop ) ) )
					return false;

				$resized_file = $editor->save();

				if ( ! is_wp_error( $resized_file ) ) {
					$resized_rel_path = str_replace( $upload_dir, '', $resized_file['path'] );
					$img_url = $upload_url . $resized_rel_path;
				} else {
					return false;
				}

			} else {

				$resized_img_path = image_resize( $img_path, $width, $height, $crop ); // Fallback foo.
				if ( ! is_wp_error( $resized_img_path ) ) {
					$resized_rel_path = str_replace( $upload_dir, '', $resized_img_path );
					$img_url = $upload_url . $resized_rel_path;
				} else {
					return false;
				}

			}

		}
	}

	// Okay, leave the ship.
	if ( true === $upscale ) remove_filter( 'image_resize_dimensions', 'aq_upscale' );

	// Return the output.
	if ( $single ) {
		// str return.
		$image = $img_url;
	} else {
		// array return.
		$image = array (
			0 => $img_url,
			1 => $dst_w,
			2 => $dst_h
		);
	}

	return $image;
}


function aq_upscale( $default, $orig_w, $orig_h, $dest_w, $dest_h, $crop ) {
	if ( ! $crop ) return null; // Let the wordpress default function handle this.

	// Here is the point we allow to use larger image size than the original one.
	$aspect_ratio = $orig_w / $orig_h;
	$new_w = $dest_w;
	$new_h = $dest_h;

	if ( ! $new_w ) {
		$new_w = intval( $new_h * $aspect_ratio );
	}

	if ( ! $new_h ) {
		$new_h = intval( $new_w / $aspect_ratio );
	}

	$size_ratio = max( $new_w / $orig_w, $new_h / $orig_h );

	$crop_w = round( $new_w / $size_ratio );
	$crop_h = round( $new_h / $size_ratio );

	$s_x = floor( ( $orig_w - $crop_w ) / 2 );
	$s_y = floor( ( $orig_h - $crop_h ) / 2 );

	return array( 0, 0, (int) $s_x, (int) $s_y, (int) $new_w, (int) $new_h, (int) $crop_w, (int) $crop_h );
}
?>