<?php 

function teo_add_simple_buttons(){ 
    wp_print_scripts( 'quicktags' );
	$output = "<script type='text/javascript'>\n
	/* <![CDATA[ */ \n";
	
	$buttons = array();
	
	$buttons[] = array('name' => 'one_third',
					'options' => array(
						'display_name' => 'one_third',
						'open_tag' => '\n[one_third]',
						'close_tag' => '[/one_third]\n',
						'key' => ''
					));
	$buttons[] = array('name' => 'one_half',
					'options' => array(
						'display_name' => 'one_half',
						'open_tag' => '\n[one_half]',
						'close_tag' => '[/one_half]\n',
						'key' => ''
					));
	$buttons[] = array('name' => 'two_thirds',
					'options' => array(
						'display_name' => 'two_thirds',
						'open_tag' => '\n[two_thirds]',
						'close_tag' => '[/two_thirds]\n',
						'key' => ''
					));
	$buttons[] = array('name' => 'one_fourth',
					'options' => array(
						'display_name' => 'one_fourth',
						'open_tag' => '\n[one_fourth]',
						'close_tag' => '[/one_fourth]\n',
						'key' => ''
					));
	$buttons[] = array('name' => 'one_column',
					'options' => array(
						'display_name' => 'one_column',
						'open_tag' => '\n[one_column]',
						'close_tag' => '[/one_column]\n',
						'key' => ''
					));
	$buttons[] = array('name' => 'full',
					'options' => array(
						'display_name' => 'full column',
						'open_tag' => '\n[full]',
						'close_tag' => '[/full]\n',
						'key' => ''
					));
	$buttons[] = array('name' => 'clear',
					'options' => array(
						'display_name' => 'clear',
						'open_tag' => '\n[clear]',
						'close_tag' => '[/clear]\n',
						'key' => ''
					));
	$buttons[] = array('name' => 'center',
					'options' => array(
						'display_name' => 'center',
						'open_tag' => '[center]',
						'close_tag' => '[/center]',
						'key' => ''
					));
	$buttons[] = array('name' => 'header',
					'options' => array(
						'display_name' => 'header',
						'open_tag' => '\n[header]',
						'close_tag' => '[/header]\n',
						'key' => ''
					));	
	$buttons[] = array('name' => 'subheader',
					'options' => array(
						'display_name' => 'subheader',
						'open_tag' => '\n[subheader]',
						'close_tag' => '[/subheader]\n',
						'key' => ''
					));
					
	for ($i=0; $i <= (count($buttons)-1); $i++) {
		$output .= "edButtons[edButtons.length] = new edButton('ed_{$buttons[$i]['name']}'
			,'{$buttons[$i]['options']['display_name']}'
			,'{$buttons[$i]['options']['open_tag']}'
			,'{$buttons[$i]['options']['close_tag']}'
			,'{$buttons[$i]['options']['key']}'
		); \n";
	}
	
	$output .= "\n /* ]]> */ \n
	</script>";
	echo $output;
}

	
add_action('admin_init', 'teo_init_shortcodes');
function teo_init_shortcodes(){
	if ( current_user_can( 'edit_posts' ) && current_user_can( 'edit_pages' ) ) {
		if ( in_array(basename($_SERVER['PHP_SELF']), array('post-new.php', 'page-new.php', 'post.php', 'page.php') ) ) {
			add_filter('mce_buttons', 'teo_filter_mce_button');
			add_filter('mce_external_plugins', 'teo_filter_mce_plugin');
			add_action('admin_head','teo_add_simple_buttons');
			add_action('edit_form_advanced', 'teo_advanced_buttons');
			add_action('edit_page_form', 'teo_advanced_buttons');
		}
	}
}

function teo_filter_mce_button($buttons) {
	array_push( $buttons, '|', 'teo_skill', 'teo_testimonials', 'teo_carousel', 'teo_service', 'teo_portfolio', 'teo_button', 'teo_team', 'teo_gallery' );

	return $buttons;
}

function teo_filter_mce_plugin($plugins) {
	$plugins['teo_quicktags'] = get_template_directory_uri() . "/teoPanel/shortcodes/js/editor_plugin.js";
	
	return $plugins;
}

function teo_advanced_buttons(){
	global $themename; echo '<style type="text/css">#TB_window { height: 100% !important; } #TB_ajaxContent { overflow: scroll !important; } </style>'; ?>
	<script type="text/javascript">
		var defaultSettings = {},
			outputOptions = '',
			selected ='',
			content = '';
		
		defaultSettings['skill'] = {
			name: {
				name: 'Skill name',
				defaultvalue: '',
				description: 'The name of the skill, e.g. Design, WordPress, etc',
				type: 'text',
			},
			value: {
				name: 'Skill value(percentage, 0-100 value, default 50)',
				defaultvalue: '',
				description: 'Please use only 0-100 values',
				type: 'text'
			},
		};

		defaultSettings['testimonials_slider'] = {
			author: {
				name: 'Author name',
				defaultvalue: '',
				description: 'The author of the testimonial',
				type: 'text',
				clone: 'cloned'
			},
			content: {
				name: 'Testimonial text',
				defaultvalue: '',
				description: 'The text of the testimonial.',
				type: 'text',
				clone: 'cloned'
			},
		};

		defaultSettings['gallery'] = {
			content: {
				name: 'Big Image URL',
				defaultvalue: '',
				description: 'The URL to the big image that will be visible via lightbox',
				type: 'text',
				clone: 'cloned'
			},
			thumb: {
				name: 'Thumbnail URL',
				defaultvalue: '',
				description: 'The URL to the thumbnail. If not used, the big image will be resized.',
				type: 'text',
				clone: 'cloned'
			},
			alt: {
				name: 'Image alt text',
				defaultvalue: '',
				description: 'Used for SEO purposes. Optional.',
				type: 'text',
				clone: 'cloned'
			},
		};

		defaultSettings['button'] = {
			content: {
				name: 'Button text',
				defaultvalue: '',
				description: 'The text of the button',
				type: 'text'
			},
			url: {
				name: 'URL of the button, if applicable',
				defaultvalue: '',
				description: 'If used, the button will link to some URL',
				type: 'text'
			},
			newwindow: {
				name: 'Open in new window?',
				defaultvalue: '',
				description: 'Should the button link open in a new window?',
				type: 'select',
				options: 'no|yes'
			},
		};

		defaultSettings['team'] = {
			image: {
				name: 'The member image',
				defaultvalue: '',
				description: 'The image of the team member',
				type: 'text'
			},
			name: {
				name: 'Member name',
				defaultvalue: '',
				description: 'The name of the team member.',
				type: 'text'
			},
			position: {
				name: 'Member position',
				defaultvalue: '',
				description: 'The position of the team member.',
				type: 'text'
			},
			description: {
				name: 'Member description',
				defaultvalue: '',
				description: 'Some words about the person.',
				type: 'text'
			},
			twitter: {
				name: 'Twitter URL',
				defaultvalue: '',
				description: 'The person\'s twitter profile URL, if applicable',
				type: 'text'
			},
			facebook: {
				name: 'Facebook URL',
				defaultvalue: '',
				description: 'The person\'s facebook profile URL, if applicable',
				type: 'text'
			},
			gplus: {
				name: 'Google+ URL',
				defaultvalue: '',
				description: 'The person\'s google+ profile URL, if applicable',
				type: 'text'
			},
			pinterest: {
				name: 'Pinterest URL',
				defaultvalue: '',
				description: 'The person\'s pinterest profile URL, if applicable',
				type: 'text'
			},
			columns: {
				name: 'The number of columns',
				defaultvalue: '3',
				description: 'The number of services per line, default is 3.',
				type: 'select',
				options: '1|2|3|4'
			}
		};

		defaultSettings['slider'] = {
			content: {
				name: 'Image URL',
				defaultvalue: '',
				description: 'The URL of the image in the slider',
				type: 'text',
				clone: 'cloned'
			},
			title: {
				name: 'Image Title',
				defaultvalue: '',
				description: 'The title of the image / project in the slider.',
				type: 'text',
				clone: 'cloned'
			},
			description: {
				name: 'Image description',
				defaultvalue: '',
				description: 'Some description for the image in the slider.',
				type: 'text',
				clone: 'cloned'
			},
			alt: {
				name: 'Image alt attribute',
				defaultvalue: '',
				description: 'The attribute that will show up on the image. Used for SEO purposes, optional.',
				type: 'text',
				clone: 'cloned'
			},
		};

		defaultSettings['service'] = {
			icon: {
				name: 'Icon URL(Optional)',
				defaultvalue: '',
				description: 'The URL of the icon image, optional.',
				type: 'text'
			},
			title: {
				name: 'Title of the services',
				defaultvalue: '',
				description: 'The title of the service',
				type: 'text'
			},
			text: {
				name: 'Text in the service area',
				defaultvalue: '',
				description: 'The text of the service',
				type: 'textarea'
			},
			columns: {
				name: 'The number of columns',
				defaultvalue: '3',
				description: 'The number of services per line, default is 3.',
				type: 'select',
				options: '1|2|3|4'
			}
		};

		defaultSettings['filterable_portfolio'] = {
			categories: {
				name: 'Ids of the categories to include',
				defaultvalue: '',
				description: 'The ids of the categories to include in the filterable portfolio, separated by commas, e.g. 1,2,3,4,5.',
				type: 'text'
			},
			number: {
				name: 'Number of portfolio posts to show',
				defaultvalue: '',
				description: 'The number of portfolio posts to show, default is 6.',
				type: 'text'
			},
		};
		
		function CustomButtonClick(tag){
			
			var index = tag;
			
				for (var index2 in defaultSettings[index]) {
					if (defaultSettings[index][index2]['clone'] === 'cloned')
						outputOptions += '<tr class="cloned">\n';
					else if (index === 'button' && index2 === 'icon')
						outputOptions += '<tr class="hidden">\n';
					else
						outputOptions += '<tr>\n';
					outputOptions += '<th><label for="teo-' + index2 + '">'+ defaultSettings[index][index2]['name'] +'</label></th>\n';
					outputOptions += '<td>';
					
					if (defaultSettings[index][index2]['type'] === 'select') {
						var optionsArray = defaultSettings[index][index2]['options'].split('|');
						
						outputOptions += '\n<select name="teo-'+index2+'" id="teo-'+index2+'">\n';
						
						for (var index3 in optionsArray) {
							selected = (optionsArray[index3] === defaultSettings[index][index2]['defaultvalue']) ? ' selected="selected"' : '';
							outputOptions += '<option value="'+optionsArray[index3]+'"'+ selected +'>'+optionsArray[index3]+'</option>\n';
						}
						
						outputOptions += '</select>\n';
					}
					
					if (defaultSettings[index][index2]['type'] === 'text') {
						cloned = '';
						if (defaultSettings[index][index2]['clone'] === 'cloned') cloned = "[]";
						outputOptions += '\n<input type="text" name="teo-'+index2+cloned+'" id="teo-'+index2+'" value="'+defaultSettings[index][index2]['defaultvalue']+'" />\n';
					}
					
					if (defaultSettings[index][index2]['type'] === 'textarea') {
						cloned = '';
						if (defaultSettings[index][index2]['clone'] === 'cloned') cloned = "[]";
						outputOptions += '<textarea name="teo-'+index2+cloned+'" id="teo-'+index2+'" cols="40" rows="10">'+defaultSettings[index][index2]['defaultvalue']+'</textarea>';
					}
					
					outputOptions += '\n<br/><small>'+ defaultSettings[index][index2]['description'] +'</small>';
					outputOptions += '\n</td>';
					
				}
			
		
			var width = jQuery(window).width(),
				tbHeight = jQuery(window).height(),
				tbWidth = ( 720 < width ) ? 720 : width;
			
			tbWidth = tbWidth - 80;
			tbHeight = tbHeight - 84;

			var tbOptions = "<div id='teo_shortcodes_div'><form id='teo_shortcodes'><table id='shortcodes_table' class='form-table teo-"+ tag +"'>";
			tbOptions += outputOptions;
			tbOptions += '</table>\n<p class="submit">\n<input type="button" id="shortcodes-submit" class="button-primary" value="Ok" name="submit" /></p>\n</form></div>';
			
			var form = jQuery(tbOptions);
			
			var table = form.find('table');
			form.appendTo('body').hide();

			$morelink = '';
						
			if (tag === 'gallery') {
				$morelink = jQuery('<p><a href="#" id="teo_add_more_link">Add One More gallery image</a></p>').appendTo('form#teo_shortcodes tbody');
			}

			if (tag === 'testimonials_slider') {
				$morelink = jQuery('<p><a href="#" id="teo_add_more_link">Add One More testimonial</a></p>').appendTo('form#teo_shortcodes tbody');
			}

			if (tag === 'slider') {
				$morelink = jQuery('<p><a href="#" id="teo_add_more_link">Add One More slider image</a></p>').appendTo('form#teo_shortcodes tbody');
			}

			if($morelink != '') {
				$moreSkillsLink = jQuery('a#teo_add_more_link');
				
				$moreSkillsLink.on('click',function() {
					var clonedElements = jQuery('form#teo_shortcodes .cloned');
										
					if(tag === 'testimonials_slider') { 
						//we have only 2 fields, text and author of the testimonial for this one
						newElements = clonedElements.slice(0,2).clone();
								
						var cloneNumber = clonedElements.length,
							labelNum = cloneNumber / 2;
					}
					else if(tag === 'slider') { 
						//we have 4 fields for the slider
						newElements = clonedElements.slice(0,4).clone();
								
						var cloneNumber = clonedElements.length,
							labelNum = cloneNumber / 4;
					}
					else {
						newElements = clonedElements.slice(0,3).clone();
								
						var cloneNumber = clonedElements.length,
							labelNum = cloneNumber / 3;
					}
					
					newElements.each(function(index){
						if ( index === 0 ) jQuery(this).css({'border-top':'1px solid #eeeeee'});
						
						var label = jQuery(this).find('label').attr('for'),
							newLabel = label + labelNum;
					
						jQuery(this).find('label').attr('for',newLabel);
						jQuery(this).find('input, textarea').attr('id',newLabel);
					});
					
					newElements.appendTo('form#teo_shortcodes tbody');
					$moreSkills.appendTo('form#teo_shortcodes tbody');
					return false;
				});		
			}
			
			
			form.find('#shortcodes-submit').click(function(){
							
				var shortcode = '['+tag;
								
				for( var index in defaultSettings[tag]) {
					var value = table.find('#teo-' + index).val();
					if (index === 'content') { 
						content = value;
						continue;
					}
					
					if (defaultSettings[tag][index]['clone'] !== undefined) {
						content = 'cloned';
						continue;
					} 
					
					if ( value !== defaultSettings[tag][index]['defaultvalue'] )
						shortcode += ' ' + index + '="' + value + '"';
						
				}

				shortcode += '] ' + "\n";
				
				if (content != '') {
					
					if (tag === 'testimonials_slider') {
					
						var $teo_form = jQuery('form#teo_shortcodes'),
							tabsOutput = '';
												
						var count = $teo_form.find("input[name='teo-author[]']").size();

						tabsOutput += '[testimonial ';
						var temp = $teo_form.find("input[id='teo-author']");
						if(temp.val() !== '')
							tabsOutput += 'author="' + temp.val() + '" ';
						
						tabsOutput += ']';
						var temp = $teo_form.find("input[id='teo-content']");
						if(temp.val() !== '')
							tabsOutput += temp.val();
						tabsOutput += '[/testimonial] ';

						for(i=1; i<count; i++) {
							tabsOutput += '[testimonial ';
							var temp = $teo_form.find("input[id='teo-author" + i + "']");
							if(temp.val() !== '')
								tabsOutput += 'author="' + temp.val() + '" ';
							
							tabsOutput += ']';

							var temp = $teo_form.find("input[id='teo-content" + i + "']");
							if(temp.val() !== '')
								tabsOutput += temp.val();

							tabsOutput += '[/testimonial] ';
						}
						
						content = tabsOutput;
					}

					if (tag === 'slider') {
					
						var $teo_form = jQuery('form#teo_shortcodes'),
							tabsOutput = '';
												
						var count = $teo_form.find("input[name='teo-content[]']").size();

						var temp = $teo_form.find("input[id='teo-content']");
						if(temp.val() !== '')
						{
							tabsOutput += '[slider_img ';

						
							var temp3 = $teo_form.find("input[id='teo-title']");
							if(temp3.val() !== '')
								tabsOutput += 'title="' + temp3.val() + '" ';

							temp3 = $teo_form.find("input[id='teo-description']");
							if(temp3.val() !== '')
								tabsOutput += 'description="' + temp3.val() + '" ';

							temp3 = $teo_form.find("input[id='teo-alt']");
							if(temp3.val() !== '')
								tabsOutput += 'alt="' + temp3.val() + '" ';

						
							tabsOutput += ']';

							tabsOutput += temp.val();

							tabsOutput += '[/slider_img] ';

							for(i=1; i<count; i++) {
								tabsOutput += '[slider_img ';
								var temp2 = $teo_form.find("input[id='teo-title" + i + "']");
								if(temp2.val() !== '')
									tabsOutput += 'title="' + temp2.val() + '" ';
								
								temp2 = $teo_form.find("input[id='teo-description" + i + "']");
								if(temp2.val() !== '')
									tabsOutput += 'description="' + temp2.val() + '" ';

								temp2 = $teo_form.find("input[id='teo-alt" + i + "']");
								if(temp2.val() !== '')
									tabsOutput += 'alt="' + temp2.val() + '" ';

								tabsOutput += ']';

								temp2 = $teo_form.find("input[id='teo-content" + i + "']");
								tabsOutput += temp2.val();

								tabsOutput += '[/slider_img] ';
							}
						}
						
						
						content = tabsOutput;
					}

					if (tag === 'gallery') {
					
						var $teo_form = jQuery('form#teo_shortcodes'),
							tabsOutput = '';
												
						var count = $teo_form.find("input[name='teo-content[]']").size();

						var temp = $teo_form.find("input[id='teo-content']");
						if(temp.val() !== '')
						{
							tabsOutput += '[gallery_item ';

						
							var temp3 = $teo_form.find("input[id='teo-thumb']");
							if(temp3.val() !== '')
								tabsOutput += 'thumb="' + temp3.val() + '" ';

							temp3 = $teo_form.find("input[id='teo-alt']");
							if(temp3.val() !== '')
								tabsOutput += 'alt="' + temp3.val() + '" ';

						
							tabsOutput += ']';

							tabsOutput += temp.val();

							tabsOutput += '[/gallery_item] ';

							for(i=1; i<count; i++) {
								tabsOutput += '[gallery_item ';
								var temp2 = $teo_form.find("input[id='teo-thumb" + i + "']");
								if(temp2.val() !== '')
									tabsOutput += 'thumb="' + temp2.val() + '" ';
								
								temp2 = $teo_form.find("input[id='teo-alt" + i + "']");
								if(temp2.val() !== '')
									tabsOutput += 'alt="' + temp2.val() + '" ';

								tabsOutput += ']';

								temp2 = $teo_form.find("input[id='teo-content" + i + "']");
								tabsOutput += temp2.val();

								tabsOutput += '[/gallery_item] ';
							}
						}
						
						
						content = tabsOutput;
					}
									
					shortcode += content;
					shortcode += '[/'+tag+'] ' + "\n";
				}

				tinyMCE.activeEditor.execCommand('mceInsertContent', 0, shortcode + ' ');
				
				tb_remove();
			});
			
			tb_show( 'Teo ' +  tag + ' Shortcode', '#TB_inline?width=' + tbWidth + '&height=' + tbHeight + '&inlineId=teo_shortcodes_div' );
			jQuery('#teo_shortcodes_div').remove();
			outputOptions = '';
		}
	</script>
<?php } ?>