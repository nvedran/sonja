<?php
the_post();
$title = get_the_title();
$image1 = trim(get_post_meta($post->ID, '_portfolio_image1', true));
$image2 = trim(get_post_meta($post->ID, '_portfolio_image2', true));
$image3 = trim(get_post_meta($post->ID, '_portfolio_image3', true));
$image4 = trim(get_post_meta($post->ID, '_portfolio_image4', true));
$image5 = trim(get_post_meta($post->ID, '_portfolio_image5', true));
$description = get_post_meta($post->ID, '_portfolio_description', true);
$buttontext = get_post_meta($post->ID, '_portfolio_buttontext', true);
$buttonurl = get_post_meta($post->ID, '_portfolio_buttonurl', true);
?>
<br />
    <div class='half'>
        <div class='screenshot-slider flexslider flex-viewport'>
            <ul class='slides'>
                <?php if($image1 != '') { ?>
	                <li>
	                    <figure>
	                    	<img style="height: 340px" src="<?php echo $image1;?>" alt="<?php echo $title;?>" />
	                    </figure>
	                </li>
	            <?php } ?>
                
                <?php if($image2 != '') { ?>
	                <li>
	                    <figure>
	                    	<img style="height: 340px" src="<?php echo $image2;?>" alt="<?php echo $title;?>" />
	                    </figure>
	                </li>
	            <?php } ?>

	            <?php if($image3 != '') { ?>
	                <li>
	                    <figure>
	                    	<img style="height: 340px" src="<?php echo $image3;?>" alt="<?php echo $title;?>" />
	                    </figure>
	                </li>
	            <?php } ?>

	            <?php if($image4 != '') { ?>
	                <li>
	                    <figure>
	                    	<img style="height: 340px" src="<?php echo $image4;?>" alt="<?php echo $title;?>" />
	                    </figure>
	                </li>
	            <?php } ?>

	            <?php if($image5 != '') { ?>
	                <li>
	                    <figure>
	                    	<img style="height: 340px" src="<?php echo $image5;?>" alt="<?php echo $title;?>" />
	                    </figure>
	                </li>
	            <?php } ?>
             </ul>
        </div>
	</div>
    <div class='half' style="margin-bottom: 25px">
        <div class='project-description'>
            <p><?php echo $description;?></p>
        </div>
	    <div class='tags'>
	        <ul>
	        	<?php 
	        	$categories = get_the_category();
	        	if($categories) {
	        		foreach($categories as $category) { 
	        			echo '<li>' . $category->cat_name . '</li>';
	        		}
	        	}
	        	?>
	        </ul>
	    </div>
	    <div style="clear: both"></div>
	    <?php if($buttontext != '') { 
	    	if($buttonurl != '') 
	    		echo '<a href="' . esc_url($buttonurl) . '" class="btn custom-button">' . __('Visit Site', 'Teo') . '</a>';
	    	else
	    		echo '<a class="btn custom-button"> ' . __('Visit Site', 'Teo') . '</a>';
	    } ?>
	</div>
<script type="text/javascript">
		jQuery('.portfolio_details .screenshot-slider').flexslider();
</script>