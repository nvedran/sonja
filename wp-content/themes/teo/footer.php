        <footer>
            <div class='container'>
                <div class='row'>
                    <div class='span10 offset1'>
                        <p>&copy; <?php echo date("Y") . ' '; bloginfo('name');?>, <?php _e('Developed by ', 'Teo');?><a href="http://teothemes.com" title="WordPress themes">TeoThemes</a></p>
                    </div>
                </div>
            </div>
        </footer>
    </div>

<?php global $teo_options;
if(isset($teo_options['integration_footer'])) echo $teo_options['integration_footer'] . PHP_EOL; ?>

 <?php wp_footer(); ?>
 
</body>
</html>