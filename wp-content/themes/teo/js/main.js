var TeoSinglePage = {
    slideCounter : 0,
    init : function() {
        var menuActive = jQuery("#mainNav li.active").length;
        if(menuActive == 0) {
            var temp = jQuery("#mainNav li.current-menu-item");
            if(temp.length > 0)
                temp.addClass("active");
        }
        jQuery('#send_message').click(function(e){
    
            e.preventDefault();
            var error = false;
            var $this = jQuery(this);
            var name = $this.parent().find('#contact-name').val();
            var email = $this.parent().find('#contact-email').val();
            var message = $this.parent().find('#contact-message').val();
            var captcha = $this.parent().find('#contact-captcha').val();
        
            if(name.length === 0){
                error = true;
                jQuery('#contact-name').css('border','1px solid red');
            }
                
            if(email.length === 0 || email.indexOf('@') === '-1'){
                error = true;
                jQuery('#contact-email').css('border','1px solid red');
            }
            
            if(message.length === 0){
                error = true;
                jQuery('#contact-message').css('border','1px solid red');
            }

            if(captcha.length === 0){
                error = true;
                jQuery('#contact-captcha').css('border','1px solid red');
            }
            
            
            if(error === false){

                jQuery.post(templateDir + "/index.php", { name : name, email : email, comment : message, captcha: captcha, submit : "yes" }, function(data) {
                    jQuery('.confirmation_message').html('<h3>' + data + '</h3>') .fadeIn(500);
                    if(data == 'Your message has been sent')
                        jQuery('.custom-form').fadeOut('slow');
                });
            }
        });

        jQuery('p.form-submit #submit').addClass('btn custom-button');

        jQuery("h3#reply-title").wrap('<div class="title-container" />');

        jQuery(".portfolio-box a.add").on('click', function(e) {
            e.preventDefault();
            var url = jQuery(this).attr("href");
            jQuery.get(url, function(data) {
                jQuery(".portfolio_details").show(1000).html(data);
                var scrollTarget = jQuery(".portfolio_details").offset().top;
                jQuery('html,body').animate({scrollTop:scrollTarget-80}, 1000, "swing");
            });
        });

        jQuery('input, textarea').placeholder();

        jQuery("a[rel^='prettyPhoto']").prettyPhoto({
        });

        jQuery("#mainNav > ul").tinyNav({
            active: 'active',
            header: 'Navigation'
        });
        jQuery('.l_tinynav1').addClass('hidden-phone');
        jQuery('#tinynav1').addClass('visible-phone');

        jQuery(window).scroll(function() {
            TeoSinglePage.hideScrollToTop();
        });
        jQuery('.testimonials-box').flexslider({
            'controlNav': true,
            'directionNav' : false,
            "touch": true,
            "animation": "slide",
            "animationLoop": true,
            "slideshow" : true,
            useCSS : false
        });
        jQuery('.quote-slider').flexslider({
            'controlNav': false,
            'directionNav' : false,
            "touch": true,
            "animation": "slide",
            "animationLoop": true,
            "slideshow" : true,
            useCSS : false
        });
        jQuery('.homepage-slider').flexslider({
            'controlNav': false,
            'directionNav' : false,
            "touch": true,
            "animation": "fade",
            "animationLoop": true,
            "slideshow" : true,
            useCSS : false
        });

        jQuery('.back-to-top').click(TeoSinglePage.scrollTop);
        TeoSinglePage.activeIndicator();
        TeoSinglePage.commentsBorder();
        TeoSinglePage.resizeHomepageSlider();
        TeoSinglePage.resizeServiceBox();
        jQuery('#mainNav').on('activate', function () {
            TeoSinglePage.activeIndicatorScrollSpy();
        });

        //parallax effect

        //save selectors as variables to increase performance
        var $window = jQuery(window);
        var bg1 = jQuery("#section-parallax-1, #section-parallax-4 .bg-image");
        var bg2 = jQuery("#section-parallax-2, #section-parallax-5 .bg-image");
        var bg3 = jQuery("#section-parallax-3, #section-parallax-6 .bg-image");

        var windowHeight = $window.height(); //get the height of the window

        /*arguments:
         x = horizontal position of background
         windowHeight = height of the viewport
         pos = position of the scrollbar
         adjuster = adjust the position of the background
         inertia = how fast the background moves in relation to scrolling
         */
        function newPos(x, windowHeight, pos, adjuster, inertia){
            return x + "% " + (-((windowHeight + pos) - adjuster) * inertia)  + "px";
        }

        //function to be called whenever the window is scrolled or resized
        function Move(){
            var pos = $window.scrollTop(); //position of the scrollbar

            bg1.css({'backgroundPosition': newPos(50, windowHeight, pos, 3000, 0.2)});
            bg2.css({'backgroundPosition': newPos(70, windowHeight, pos, 4000, 0.2)});
            bg3.css({'backgroundPosition': newPos(50, windowHeight, pos, 6000, 0.2)});

        }

        $window.resize(function(){ //if the user resizes the window...
            if(jQuery(window).width() > 766) {
                Move(); //move the background images in relation to the movement of the scrollbar
            }
        });

        $window.bind('scroll', function(){ //when the user is scrolling...
            if(jQuery(window).width() > 766) {
                Move(); //move the background images in relation to the movement of the scrollbar
            }
        });

//        jQuery('body').scrollspy();
    },
    resizeHomepageSlider : function() {
        var windowH = jQuery(window).height();
        var headerH = jQuery('header').height();
        var height = windowH - headerH;
        var textH = jQuery('.homepage-slider .text-section .span12').height();
        jQuery('.homepage-slider').height((windowH - headerH));
        jQuery('.homepage-slider .slides li').height((windowH - headerH));
        jQuery('.homepage-slider .text-section .span12 .title').css({'marginTop' : (height/2 - textH/2) + "px"});

    },
    scrollTop : function() {
        jQuery('body, html').animate({
            scrollTop:  "0px"
        }, 500);
        return false;
    },
    resizeServiceBox : function() {
      jQuery('.services-box').each(function() {
          jQuery(this).height(jQuery(this).find('.services-diamond').height() + parseInt(jQuery(this).find('.services-diamond').css('marginTop')) + jQuery(this).find('.bottom').height() + 10);
      });
    },
    hideScrollToTop : function() {
        var windowH = jQuery(window).height();
        var scrollH = jQuery(window).scrollTop() + windowH - 100;
        if( windowH < scrollH ) {
            jQuery('.back-to-top').fadeIn('slow');
        } else {
            jQuery('.back-to-top').fadeOut('slow');
        }
    },
    activeIndicator : function() {
        jQuery(window).bind("load", function() {
            if(jQuery('#mainNav li.active').length == 0) 
                jQuery(".active-indicator").hide();
            else {
                var leftPos = jQuery('#mainNav li.active').position().left;
                var activeW = jQuery('#mainNav li.active').width();
                var setPosition = (activeW / 2) + leftPos - 5.5;
                jQuery('.active-indicator').css({'left' : setPosition + 'px'});
                jQuery('#mainNav li').hover(function() {
                    var leftPos = jQuery(this).position().left;
                    var activeW = jQuery(this).width();
                    var setPosition = (activeW / 2) + leftPos - 5.5;
                    jQuery('.active-indicator').animate({'left' : setPosition + 'px'}, {queue : false, duration: 300});
                }, function() {
                    var leftPos = jQuery('#mainNav li.active').position().left;
                    var activeW = jQuery('#mainNav li.active').width();
                    var setPosition = (activeW / 2) + leftPos - 5.5;
                    jQuery('.active-indicator').animate({'left' : setPosition + 'px'}, {queue : false, duration: 300});
                });
            }
        });
    },
    activeIndicatorScrollSpy : function() {
        var leftPos = jQuery('#mainNav li.active').position().left;
        var activeW = jQuery('#mainNav li.active').width();
        var setPosition = (activeW / 2) + leftPos - 5.5;
        jQuery('.active-indicator').animate({'left' : setPosition + 'px'}, {queue : false, duration: 300});
    },
    commentsBorder : function() {
        jQuery('.top-comment > .left').each(function() {
            var commentH = jQuery(this).parent().height();
            var lastComH = jQuery(this).next().find('>.last-comment').height();
            var figureH = jQuery(this).find('figure').first().height();
            jQuery(this).find('.border').height((commentH - lastComH - (figureH * 0.5) ));
        });
        jQuery('.inline-comment > .left').each(function() {
            var commentH = jQuery(this).parent().height();
            var lastComH = jQuery(this).next().find('>.last-comment').height();
            var figureH = jQuery(this).find('figure').first().height();
            jQuery(this).find('.border').height((commentH - lastComH - (figureH * 0.5) ));
        });
    }
}

jQuery(document).ready(function() {
   TeoSinglePage.init();

    function filterPath(string) {
        return string
            .replace(/^\//,'')
            .replace(/(index|default).[a-zA-Z]{3,4}$/,'')
            .replace(/\/$/,'');
    }
    var locationPath = filterPath(location.pathname);
    var scrollElem = scrollableElement('.scrollable-container, html');

    jQuery('#mainNav a[href*=#]:not([href=#])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') || location.hostname == this.hostname) {

            var target = jQuery(this.hash);
            target = target.length ? target : jQuery('[name=' + this.hash.slice(1) +']');
            if (target.length) {
                jQuery('body ,html').animate({
                    scrollTop: (target.offset().top - jQuery('header').height())
                }, 1000);
                jQuery('#mainNav li.active').removeClass('active');
                jQuery(this).parent().addClass('active');
                return false;
            }
        }
    });

    // use the first element that is "scrollable"
    function scrollableElement(els) {
        for (var i = 0, argLength = arguments.length; i <argLength; i++) {
            var el = arguments[i],
                $scrollElement = jQuery(el);
            if ($scrollElement.scrollTop()> 0) {
                return el;
            } else {
                $scrollElement.scrollTop(1);
                var isScrollable = $scrollElement.scrollTop()> 0;
                $scrollElement.scrollTop(0);
                if (isScrollable) {
                    return el;
                }
            }
        }
        return [];
    }
});

jQuery(window).resize(function() {
    TeoSinglePage.commentsBorder();
    TeoSinglePage.resizeHomepageSlider();
    TeoSinglePage.resizeServiceBox();
});