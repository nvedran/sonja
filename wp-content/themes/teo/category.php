<?php
get_header();
global $teo_options;
the_post(); 
?>
<div class='scrollable-container'>
    <section class='section-title' id='blog-page'>
        <div class='container'>
            <div class='row'>
                <div class='span12'>
                    <h1>
                        <?php _e('Posts added in ', 'Teo' ); single_cat_title( '', false ) ?>
                    </h1>
                </div>
                <div class='section-diamond'></div>
            </div>
        </div>
    </section>
    <section class='section-default section-blog'>
        <div class='container'>
            <div class='row'>
                <div class="span8">
                    <div class='inner'>
                        <?php
                        global $query_string;
                        query_posts($query_string);
                        $i=1;
                        if(have_posts() ) : while(have_posts() ) : the_post(); ?>
                            <article <?php if($i==1) post_class("first-article"); else post_class(); ?>>
                                <div class='inner-article'>
                                    <?php if(has_post_thumbnail() ) { 
                                        $thumb = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );?>
                                        <figure>
                                            <?php 
                                                $width = 641;
                                                $height = 320;
                                            ?>
                                            <img src="<?php echo aq_resize($thumb, $width, $height, true);?>" alt="<?php the_title();?>" />
                                            <a href="<?php the_permalink();?>" class='figure-link'><i class='plus-sign'></i></a>
                                        </figure>
                                    <?php } ?>
                                    <h2><a href='<?php the_permalink();?>'><?php the_title();?></a></h2>
                                    <span class='date'><?php the_time("d M, Y");?> - <?php _e('By ', 'Teo'); the_author_posts_link();?></span>
                                    <div class='excerpt'>
                                        <p><?php the_excerpt();?></p>
                                    </div>
                                    <a href="<?php the_permalink();?>" class='btn custom-button'><?php _e('Read more', 'Teo');?></a>
                                </div>
                            </article>
                        <?php $i++; endwhile; get_template_part('includes/pagination'); endif; wp_reset_query(); ?>
                    </div>
                </div>

                <aside class='span4'>
                    <?php dynamic_sidebar("Right sidebar"); ?>
                </aside>
            </div>
        </div>
    </section>

<?php get_footer();?>