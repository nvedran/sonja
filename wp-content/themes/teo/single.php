<?php 
get_header();
the_post(); 
?>
<div class='scrollable-container'>
        <section class='section-default section-blog'>
            <div class='container'>
                <div class='row'>
                    <div class='span8'>
                        <div class='inner'>
                            <article class='first-article'>
                                <div class='inner-article'>
                                    <?php if(has_post_thumbnail() ) { 
                                        $thumb = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );?>
                                        <figure>
                                            <?php 
                                                $width = 641;
                                                $height = 320;
                                            ?>
                                            <img src="<?php echo aq_resize($thumb, $width, $height, true);?>" alt="<?php the_title();?>" />
                                        </figure>
                                    <?php } ?>
                                    <h2><a><?php the_title();?></a></h2>
                                    <span class='date'><?php the_time("d M, Y");?> - <?php _e('By ', 'Teo'); the_author_posts_link();?></span>
                                    <div class='article-content'>
                                        <?php the_content();?>
                                        <?php wp_link_pages(array('before' => '<p><strong>'.esc_html__('Pages','Teo').':</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>
                                    </div>
                                     <div class="clear"></div>
                                    <div class="tags">
                                         <?php the_tags('<div class="btn custom-button">', '</div> <div class="btn custom-button">', '</div><br />'); ?> 
                                      </div>
                                     <div class="clear"></div>
                                     <br />
                                    <div class="single-navigation">
                                        <?php previous_post_link("<div class='prev'>&laquo; %link</div>"); ?>
                                        <?php next_post_link("<div class='next'>%link &raquo;</div>"); ?> 
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class='article-section'>
                                    <div class='title-container'>
                                        <h2><?php _e('Share it!', 'Teo');?></h2>
                                    </div>
                                    <div class='article-section-content'>
                                        <!-- AddThis Button BEGIN -->
                                        <div class="addthis_toolbox addthis_default_style ">
                                        <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
                                        <a class="addthis_button_tweet"></a>
                                        <a class="addthis_button_pinterest_pinit"></a>
                                        <a class="addthis_counter addthis_pill_style"></a>
                                        </div>
                                        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=xa-5133cbfc3c9054b8"></script>
                                        <!-- AddThis Button END -->
                                    </div>
                                </div>
                                <?php comments_template('', true);?>
                            </article>
                        </div>
                    </div>
                    <aside class='span4'>
                        <?php dynamic_sidebar("Right sidebar"); ?>
                    </aside>
                </div>
            </div>
        </section>

<?php get_footer();?>