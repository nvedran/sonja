<?php

// Tabbed Widget
class Tabbed extends WP_Widget
{
    function Tabbed(){
    $widget_ops = array('description' => 'Tabbed widget showing the most popular, recent and commented posts.');
    $control_ops = array('width' => 200, 'height' => 300);
    parent::__construct(false,$name='[Teo] Tabbed widget',$widget_ops,$control_ops);
    }

  /* Displays the Widget in the front-end */
    function widget($args, $instance){
    extract($args);
    $number_posts = $instance['number_posts'];
    $id = rand(1, 50000);

    echo $before_widget; ?>

    <div class="tabbable">
        <ul class="nav nav-tabs">
            <li class="first-child active">
                <a data-toggle="tab" href="#tab1_<?php echo $id;?>"><div class="inner-tab"><?php _e('Popular', 'Teo')?></div></a>
            </li>
            <li>
                <a data-toggle="tab" href="#tab2_<?php echo $id;?>"><div class="inner-tab"><?php _e('Recent', 'Teo')?></div></a>
            </li>
            <li>
                <a data-toggle="tab" href="#tab3_<?php echo $id;?>"><div class="inner-tab"><?php _e('Random', 'Teo')?></div></a>
            </li>
        </ul>
        <div class="tab-content">
            <div id="tab1_<?php echo $id;?>" class="tab-pane active">
                <ul class='post-list'>
                    <?php 
                    $args = array();
                    $args['posts_per_page'] = $number_posts;
                    $args['orderby'] = 'comment_count';
                    $args['order'] = 'DESC';
                    $query = new WP_Query($args);
                    while($query->have_posts() ) : $query->the_post(); global $post; ?>
                        <li>
                                <figure>
                                    <a title="<?php the_title();?>" href="<?php the_permalink();?>">
                                        <?php if(has_post_thumbnail() ) { ?>
                                            <img src="<?php echo aq_resize(wp_get_attachment_url( get_post_thumbnail_id($post->ID) ), 90, 90, true); ?>" alt="<?php the_title();?>" />
                                        <?php } else { ?>
                                            <img src="<?php echo get_template_directory_uri() . '/img/missing_56.png';?>" alt="<?php the_title();?>" />
                                        <?php } ?>
                                    </a>
                                </figure>
                                <span class='title'>
                                    <a href="<?php the_permalink();?>"><?php the_title();?></a> 
                                </span>
                                <span class='details'><?php the_time("F d, Y");?>, <?php _e('by ', 'Teo'); the_author_posts_link();?></span>
                        </li>
                    <?php endwhile; wp_reset_postdata(); ?>
                </ul>
            </div>
            <div id="tab2_<?php echo $id;?>" class="tab-pane">
                <ul class='post-list'>
                    <?php 
                    $args = array();
                    $args['posts_per_page'] = $number_posts;
                    $query = new WP_Query($args);
                    while($query->have_posts() ) : $query->the_post(); global $post ?>
                        <li>
                                <figure>
                                    <a title="<?php the_title();?>" href="<?php the_permalink();?>">
                                        <?php if(has_post_thumbnail() ) { ?>
                                            <img src="<?php echo aq_resize(wp_get_attachment_url( get_post_thumbnail_id($post->ID) ), 90, 90, true); ?>" alt="<?php the_title();?>" />
                                        <?php } else { ?>
                                            <img src="<?php echo get_template_directory_uri() . '/img/missing_56.png';?>" alt="<?php the_title();?>" />
                                        <?php } ?>
                                    </a>
                                </figure>
                                <span class='title'>
                                    <a href="<?php the_permalink();?>"><?php the_title();?></a> 
                                </span>
                                <span class='details'><?php the_time("F d, Y");?>, <?php _e('by ', 'Teo'); the_author_posts_link();?></span>
                        </li>
                    <?php endwhile; wp_reset_postdata(); ?>

                </ul>
            </div>
            <div id="tab3_<?php echo $id;?>" class="tab-pane">
                <ul class='post-list'>
                    <?php 
                    $args = array();
                    $args['posts_per_page'] = $number_posts;
                    $args['orderby'] = 'rand';
                    $query = new WP_Query($args);
                    while($query->have_posts() ) : $query->the_post(); global $post; ?>
                        <li>
                                <figure>
                                    <a title="<?php the_title();?>" href="<?php the_permalink();?>">
                                        <?php if(has_post_thumbnail() ) { ?>
                                            <img src="<?php echo aq_resize(wp_get_attachment_url( get_post_thumbnail_id($post->ID) ), 90, 90, true); ?>" alt="<?php the_title();?>" />
                                        <?php } else { ?>
                                            <img src="<?php echo get_template_directory_uri() . '/img/missing_56.png';?>" alt="<?php the_title();?>" />
                                        <?php } ?>
                                    </a>
                                </figure>
                                <span class='title'>
                                    <a href="<?php the_permalink();?>"><?php the_title();?></a> 
                                </span>
                                <span class='details'><?php the_time("F d, Y");?>, <?php _e('by ', 'Teo'); the_author_posts_link();?></span>
                        </li>
                    <?php endwhile; wp_reset_postdata(); ?>

                </ul>
            </div>
        </div>
    </div>

    <?php
    echo $after_widget;
  }

  /*Saves the settings. */
    function update($new_instance, $old_instance){
    $instance =  array();
    $instance['number_posts'] = (int)$new_instance['number_posts'];

    return $instance;
  }

  /*Creates the form for the widget in the back-end. */
    function form($instance){
    //Defaults
    $instance = wp_parse_args( (array) $instance, array('number_posts'=> 5) );

    $number_posts = (int) $instance['number_posts'];
    echo '<p><label for="' . $this->get_field_id('number_posts') . '">' . 'Number of posts: ' . '</label><input id="' . $this->get_field_id('number_posts') . '" name="' . $this->get_field_name('number_posts') . '" value="'. esc_textarea($number_posts)  . '" /></p>';
  }

}// end Tabbed class

// PopularPosts Widget
class PopularPosts extends WP_Widget
{
    function PopularPosts(){
    $widget_ops = array('description' => 'Shows the most commented posts.');
    $control_ops = array('width' => 200, 'height' => 300);
    parent::__construct(false,$name='[Teo] Popular posts',$widget_ops,$control_ops);
    }

  /* Displays the Widget in the front-end */
    function widget($args, $instance){
    extract($args);

    $title = apply_filters('widget_title', empty($instance['title']) ? '' : $instance['title']);
    $number_posts = $instance['number_posts'];
    $categories = $instance['categories'];

    echo $before_widget; 

    if ( $title != '' )
        echo $before_title . $title . $after_title;

    echo '
        <ul class="post-list">';
    $args = array();
    $args['posts_per_page'] = $number_posts;
    $args['orderby'] = 'comment_count';
    $args['order'] = 'DESC';
    if(!empty($categories) )
        $args['category__in'] = $categories;
    $query = new WP_Query($args);
    while($query->have_posts() ) : $query->the_post(); global $post; ?>
        <li>
                <figure>
                    <a title="<?php the_title();?>" href="<?php the_permalink();?>">
                        <?php if(has_post_thumbnail() ) { ?>
                            <img src="<?php echo aq_resize(wp_get_attachment_url( get_post_thumbnail_id($post->ID) ), 90, 90, true); ?>" alt="<?php the_title();?>" />
                        <?php } else { ?>
                            <img src="<?php echo get_template_directory_uri() . '/img/missing_56.png';?>" alt="<?php the_title();?>" />
                        <?php } ?>
                    </a>
                </figure>
                <span class='title'>
                    <a href="<?php the_permalink();?>"><?php the_title();?></a> 
                </span>
                <span class='details'><?php the_time("F d, Y");?>, <?php _e('by ', 'Teo'); the_author_posts_link();?></span>
        </li>
    <?php endwhile; wp_reset_postdata();
    echo '</ul>';

    echo $after_widget;
  }

  /*Saves the settings. */
    function update($new_instance, $old_instance){
    $instance =  array();
    $instance['title'] = esc_attr($new_instance['title']);
    $instance['number_posts'] = (int)$new_instance['number_posts'];
    $instance['categories'] = $new_instance['categories'];

    return $instance;
  }

  /*Creates the form for the widget in the back-end. */
    function form($instance){
    //Defaults
    $instance = wp_parse_args( (array) $instance, array('number_posts'=> 5, 'title' => '', 'categories' => array() ) );

    $title = esc_attr($instance['title']);
    $number_posts = (int) $instance['number_posts'];
    $categories = (array) $instance['categories'];
    
    echo '<p><label for="' . $this->get_field_id('title') . '">' . 'Title: ' . '</label><input id="' . $this->get_field_id('title') . '" name="' . $this->get_field_name('title') . '" value="'. esc_textarea($title)  . '" /></p>';
    echo '<p><label for="' . $this->get_field_id('number_posts') . '">' . 'Number of posts: ' . '</label><input id="' . $this->get_field_id('number_posts') . '" name="' . $this->get_field_name('number_posts') . '" value="'. esc_textarea($number_posts)  . '" /></p>';
    $cats = get_categories('hide_empty=0');
    ?>
    <p>
        <label>Categories to include: </label> <br />
        <?php foreach( $cats as $category ) { ?>
            <label>
                <input type="checkbox" name="<?php echo $this->get_field_name('categories'); ?>[]" value="<?php echo $category->cat_ID; ?>"  <?php if(in_array( $category->cat_ID, $categories ) ) echo 'checked="checked" '; ?> /> 
                <?php echo $category->cat_name; ?>
            </label> <br />
        <?php } ?>
    </p> 
    <?php
  }

}// end PopularPosts class

// RandomPosts Widget
class RandomPosts extends WP_Widget
{
    function RandomPosts(){
    $widget_ops = array('description' => 'Shows some random posts from your website.');
    $control_ops = array('width' => 200, 'height' => 300);
    parent::__construct(false,$name='[Teo] Random posts',$widget_ops,$control_ops);
    }

  /* Displays the Widget in the front-end */
    function widget($args, $instance){
    extract($args);

    $title = apply_filters('widget_title', empty($instance['title']) ? '' : $instance['title']);
    $number_posts = $instance['number_posts'];

    echo $before_widget; 

    if ( $title != '' )
        echo $before_title . $title . $after_title;

    echo '
    <ul class="post-list">';
    $args = array();
    $args['posts_per_page'] = $number_posts;
    $args['orderby'] = 'rand';
    $query = new WP_Query($args);
    while($query->have_posts() ) : $query->the_post(); global $post; ?>
        <li>
                <figure>
                    <a title="<?php the_title();?>" href="<?php the_permalink();?>">
                        <?php if(has_post_thumbnail() ) { ?>
                            <img src="<?php echo aq_resize(wp_get_attachment_url( get_post_thumbnail_id($post->ID) ), 90, 90, true); ?>" alt="<?php the_title();?>" />
                        <?php } else { ?>
                            <img src="<?php echo get_template_directory_uri() . '/img/missing_56.png';?>" alt="<?php the_title();?>" />
                        <?php } ?>
                    </a>
                </figure>
                <span class='title'>
                    <a href="<?php the_permalink();?>"><?php the_title();?></a> 
                </span>
                <span class='details'><?php the_time("F d, Y");?>, <?php _e('by ', 'Teo'); the_author_posts_link();?></span>
        </li>
    <?php endwhile; wp_reset_postdata();
    echo '</ul>';

    echo $after_widget;
  }

  /*Saves the settings. */
    function update($new_instance, $old_instance){
    $instance =  array();
    $instance['title'] = esc_attr($new_instance['title']);
    $instance['number_posts'] = (int)$new_instance['number_posts'];

    return $instance;
  }

  /*Creates the form for the widget in the back-end. */
    function form($instance){
    //Defaults
    $instance = wp_parse_args( (array) $instance, array('number_posts'=> 5, 'title' => '' ) );

    $title = esc_attr($instance['title']);
    $number_posts = (int) $instance['number_posts'];
    
    echo '<p><label for="' . $this->get_field_id('title') . '">' . 'Title: ' . '</label><input id="' . $this->get_field_id('title') . '" name="' . $this->get_field_name('title') . '" value="'. esc_textarea($title)  . '" /></p>';
    echo '<p><label for="' . $this->get_field_id('number_posts') . '">' . 'Number of posts: ' . '</label><input id="' . $this->get_field_id('number_posts') . '" name="' . $this->get_field_name('number_posts') . '" value="'. esc_textarea($number_posts)  . '" /></p>';
  }

}// end RandomPosts class

// RecentPosts Widget
class RecentPosts extends WP_Widget
{
    function RecentPosts(){
    $widget_ops = array('description' => 'Shows the most recent posts from the specified categories.');
    $control_ops = array('width' => 200, 'height' => 300);
    parent::__construct(false,$name='[Teo] Recent posts',$widget_ops,$control_ops);
    }

  /* Displays the Widget in the front-end */
    function widget($args, $instance){
    extract($args);

    $title = apply_filters('widget_title', empty($instance['title']) ? '' : $instance['title']);
    $number_posts = $instance['number_posts'];
    $categories = $instance['categories'];

    echo $before_widget; 

    if ( $title != '' )
        echo $before_title . $title . $after_title;

    echo '
    <ul class="post-list">';
    $args = array();
    $args['posts_per_page'] = $number_posts;
    if(!empty($categories) )
        $args['category__in'] = $categories;
    $query = new WP_Query($args);
    while($query->have_posts() ) : $query->the_post(); global $post; ?>
        <li>
                <figure>
                    <a title="<?php the_title();?>" href="<?php the_permalink();?>">
                        <?php if(has_post_thumbnail() ) { ?>
                            <img src="<?php echo aq_resize(wp_get_attachment_url( get_post_thumbnail_id($post->ID) ), 90, 90, true); ?>" alt="<?php the_title();?>" />
                        <?php } else { ?>
                            <img src="<?php echo get_template_directory_uri() . '/img/missing_56.png';?>" alt="<?php the_title();?>" />
                        <?php } ?>
                    </a>
                </figure>
                <span class='title'>
                    <a href="<?php the_permalink();?>"><?php the_title();?></a> 
                </span>
                <span class='details'><?php the_time("F d, Y");?>, <?php _e('by ', 'Teo'); the_author_posts_link();?></span>
        </li>
    <?php endwhile; wp_reset_postdata();
    echo '</ul>';

    echo $after_widget;
  }

  /*Saves the settings. */
    function update($new_instance, $old_instance){
    $instance =  array();
    $instance['title'] = esc_attr($new_instance['title']);
    $instance['number_posts'] = (int)$new_instance['number_posts'];
    $instance['categories'] = $new_instance['categories'];

    return $instance;
  }

  /*Creates the form for the widget in the back-end. */
    function form($instance){
    //Defaults
    $instance = wp_parse_args( (array) $instance, array('number_posts'=> 5, 'title' => '', 'categories' => array() ) );

    $title = esc_attr($instance['title']);
    $number_posts = (int) $instance['number_posts'];
    $categories = (array) $instance['categories'];
    
    echo '<p><label for="' . $this->get_field_id('title') . '">' . 'Title: ' . '</label><input id="' . $this->get_field_id('title') . '" name="' . $this->get_field_name('title') . '" value="'. esc_textarea($title)  . '" /></p>';
    echo '<p><label for="' . $this->get_field_id('number_posts') . '">' . 'Number of posts: ' . '</label><input id="' . $this->get_field_id('number_posts') . '" name="' . $this->get_field_name('number_posts') . '" value="'. esc_textarea($number_posts)  . '" /></p>';
    $cats = get_categories('hide_empty=0');
    ?>
    <p>
        <label>Categories to include: </label> <br />
        <?php foreach( $cats as $category ) { ?>
            <label>
                <input type="checkbox" name="<?php echo $this->get_field_name('categories'); ?>[]" value="<?php echo $category->cat_ID; ?>"  <?php if(in_array( $category->cat_ID, $categories ) ) echo 'checked="checked" '; ?> /> 
                <?php echo $category->cat_name; ?>
            </label> <br />
        <?php } ?>
    </p> 
    <?php
  }

}// end RecentPosts class

function TeoWidgets() {
  register_widget('Tabbed');
  register_widget('PopularPosts');
  register_widget('RandomPosts');
  register_widget('RecentPosts');
}

add_action('widgets_init', 'TeoWidgets');