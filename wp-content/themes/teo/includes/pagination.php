<div class="center">
	<div class="pagination">
	<?php
	if(function_exists('wp_pagenavi')) 
		wp_pagenavi(); 
	else { ?>
			<ul>
				<li class="leftpag"><?php next_posts_link(esc_attr__('&laquo; Older Entries', 'SCRN')); ?></li>
				<li class="rightpag"><?php previous_posts_link(esc_attr__('Newer Entries &raquo;', 'SCRN')); ?></li>
			</ul>
	<?php } ?>
	</div>
</div>