<?php
add_shortcode('one_third','teo_one_third');
function teo_one_third($atts, $content = null){
	extract(shortcode_atts(array(
		'offset' => 0,
	), $atts));
	$class = '';
	if($offset != 0)
		$class = 'offset' . $offset;
	$output = '<div class="span4 ' . $class . '">' . do_shortcode($content) . '</div>';
	return $output;
}
add_shortcode('one_half','teo_one_half');
function teo_one_half($atts, $content = null){
	extract(shortcode_atts(array(
		'offset' => 0,
	), $atts));
	$class = '';
	if($offset != 0)
		$class = 'offset' . $offset;
	$output = '<div class="span6 ' . $class . '">' . do_shortcode($content) . '</div>';
	return $output;
}

add_shortcode('two_thirds','teo_two_thirds');
function teo_two_thirds($atts, $content = null){
	extract(shortcode_atts(array(
		'offset' => 0,
	), $atts));
	$class = '';
	if($offset != 0)
		$class = 'offset' . $offset;

	$output = '<div class="span8 ' . $class . '">' . do_shortcode($content) . '</div>';
	return $output;
}

add_shortcode('one_fourth','teo_one_fourth');
function teo_one_fourth($atts, $content = null){
	extract(shortcode_atts(array(
		'offset' => 0,
	), $atts));
	$class = '';
	if($offset != 0)
		$class = 'offset' . $offset;

	$output = '<div class="span3 ' . $class . '">' . do_shortcode($content) . '</div>';
	return $output;
}
add_shortcode('one_column','teo_one_column');
add_shortcode('full','teo_one_column');
function teo_one_column($atts, $content = null){
	extract(shortcode_atts(array(
		'offset' => 1,
	), $atts));
	$class = '';
	if($offset != 0)
		$class = 'offset' . $offset;

	$output = '<div class="span10 ' . $class . '">' . do_shortcode($content) . '</div>';
	return $output;
}
add_shortcode('skill','teo_skill');
function teo_skill($atts, $content = null){
	extract(shortcode_atts(array(
		'value' => '50',
		'name' => ''
	), $atts));
	if($content != '')
		$name = $content;
	$value = (int)$value;
	if($value < 1 || $value >= 100)
		$value = 50;
	$output = '<span class="progress-info">' . $name . '</span>';
	$output .= '<div class="progress">
        <div style="width: ' . $value . '%;" class="bar"></div>
    </div>';
	return $output;
}

add_shortcode('testimonials_slider', 'teo_quote_slider');
function teo_quote_slider($atts, $content=null) {
	
	$output = "<div class='testimonials-box'>
                            <ul class='slides'>";
    $output .= do_shortcode($content);
    $output .= '</ul>
    </div>';
    return $output;
}
add_shortcode('testimonial', 'teo_quote');
function teo_quote($atts, $content=null) {
	extract(shortcode_atts(array(
		'author' => ''
	), $atts));

	$output = '<li>
		<p> ' . $content . '</p>';
	if($author != '')
		$output .= '<span class="author">- ' . $author . '</span>';
	$output .= '</li>';

    return $output;
}

add_shortcode('slider', 'teo_slider');
function teo_slider($atts, $content=null) {	
	$id = rand(1, 50000);
	$output = "<div class='span8 offset2'>
                        <div id='portfolioSlider' class='portfolioSlider_$id'>
                            <ul class='slides'>";
	$output .= do_shortcode($content);
	$output .= '</ul></div>
	</div>
	<div style="clear: both"></div>';
	$output .= '
        <script type="text/javascript">
        jQuery(document).ready(function() {
        	jQuery(".portfolioSlider_' . $id . '").flexslider({
	            "controlNav": false,
	            "directionNav" : false,
	            "touch": true,
	            "animation": "slide",
	            "animationLoop": true,
	            "slideshow" : true,
	            keyboard: true,
	            multipleKeyboard: true,
	            drag : false,
	            useCSS : false
	        });
	        jQuery(".portfolioSlider_' . $id . '").delegate("li", "click", function () {
	            var activeIndex = jQuery(".portfolioSlider_' . $id . ' li.flex-active-slide").index();
	            var clickIndex = jQuery(this).index();
	            if(activeIndex > clickIndex) {
	                jQuery(".portfolioSlider_' . $id . '").flexslider("prev");
	            }
	            if(activeIndex < clickIndex) {
	                jQuery(".portfolioSlider_' . $id . '").flexslider("next");
	            }
	        });
		});
		</script>';
	return $output;
}

add_shortcode('slider_img', 'teo_slider_img');
function teo_slider_img($atts, $content=null) {
	extract(shortcode_atts(array(
		'title' => '',
		'description' => '',
		'alt' => ''
	), $atts));
	
	if($content != '')
	{
		$output = '<li>';
		$output .= '<img src="' . esc_url($content) . '" alt="' . $alt . '" />';
		$output .= '<div class="border">
            <a class="switcher">Switcher</a>
        </div>';
        if($title != '') 
        	$output .= '<h3>' . $title . '</h3>';
        if($description != '')
        	$output .= '<h4>' . $description . '</h4>';
        $output .= '</li>';
		return $output;
	}
	else return '';
}

add_shortcode('filterable_portfolio', 'teo_filterable_portfolio');
function teo_filterable_portfolio($atts, $content=null) {
	extract(shortcode_atts(array(
		'categories' => '',
		'number' => 6
	), $atts));
	global $post;
	$pid = rand(1, 50000);
	$categories = esc_attr($categories);
	$categories = str_replace(' ', '', $categories);
	$output = '<div class="portfolio-details portfolio-details-' . $pid . '">
		<div class="portfolio-menu">
					<ul>
						<li class="active"><a href="#" data-filter="*">All</a></li>';
	if($categories == '')
	{
		$cats = get_categories();
		foreach($cats as $cat) {
			$output .= '<li><a href="" data-filter=".' . $cat->term_id . '">' . $cat->name . '</a></li>';
		}
	}
	else
	{
		$cats = explode(",", $categories);
		foreach($cats as $cat) {
			$cat_details = get_category($cat);
			$output .= '<li><a href="" data-filter=".' . $cat . '">' . $cat_details->name . '</a></li>';
		}
	}

	$output .= '</ul>
			</div> 
	<div class="clear"></div>';
	$output .= '<div class="portfolio_details"></div>
	<div style="clear: both"></div>';
	$categories = trim($categories);
	$number = (int)$number;
	if($number == 0)
		$number = 9;
	$output .= '
	<div class="projects-slider">';
	$query = new WP_Query('post_type=portfolio&posts_per_page=' . $number . '&cat=' . $categories);
	while($query->have_posts() ) : $query->the_post();
		$id = rand(1, 50000);
		$title = get_the_title();
		$image1 = trim(get_post_meta($post->ID, '_portfolio_image1', true));
		$image2 = trim(get_post_meta($post->ID, '_portfolio_image2', true));
		$image3 = trim(get_post_meta($post->ID, '_portfolio_image3', true));
		$image4 = trim(get_post_meta($post->ID, '_portfolio_image4', true));
		$image5 = trim(get_post_meta($post->ID, '_portfolio_image5', true));
		$description = get_post_meta($post->ID, '_portfolio_description', true);
		$buttontext = get_post_meta($post->ID, '_portfolio_buttontext', true);
		$buttonurl = get_post_meta($post->ID, '_portfolio_buttonurl', true);
		$thumbnail = get_post_meta($post->ID, '_portfolio_thumb', true);
		$video = get_post_meta($post->ID, '_portfolio_video', true);
		if($image1 != '')
		{ 
			if($thumbnail == '')
				$thumbnail = $image1;
			$class = '';
			$cats = get_the_category();
			foreach($cats as $cat)
				$class .= $cat->term_id . ' ';
			$class .= '"';
			$output .= '<div class="portfolio-box ' . $class . '>';
			if($video != '') //if the video field is not empty, we will show the video upon clicking on the zoom icon
				$zoomlink = $video;
			else
				$zoomlink = $image1;
			$output .= '
				<figure>
	                <img src="' . $thumbnail . '" alt=""/>
	                <a href="' . get_permalink() . '" class="add" rel="project">
	                	<i></i>
	                </a>
	                <a href="' . $zoomlink . '" rel="prettyPhoto[gallery' . $id . ']" class="fullscreen"><i></i></a>';
	        if($image2 != '')
	        	$output .= '<a style="display: none" rel="prettyPhoto[gallery' . $id . ']" href="' . $image2 . '"> </a>';
	        if($image3 != '')
	        	$output .= '<a style="display: none" rel="prettyPhoto[gallery' . $id . ']" href="' . $image3 . '"> </a>';
	        if($image4 != '')
	        	$output .= '<a style="display: none" rel="prettyPhoto[gallery' . $id . ']" href="' . $image4 . '"> </a>';
	        if($image5 != '')
	        	$output .= '<a style="display: none" rel="prettyPhoto[gallery' . $id . ']" href="' . $image5 . '"> </a>';
	        $output .= '</figure>
				<h3>' . $title . '</h3>
				<p>' . substr($description, 0, 130) . '...</p>
				<span class="arrow-icon"></span>';
			$output .= '</div>';
		}
		endwhile; wp_reset_postdata();
	$output .= '</div>
	</div>
	<div class="clear"></div>';
	$output .= '
	<script type="text/javascript">
		jQuery(document).ready(function() {
			var isotopeContainer = jQuery(".portfolio-details-' . $pid . ' .projects-slider");
		        isotopeContainer.isotope({
		            itemSelector : ".portfolio-box",
		            layoutMode : "fitRows"
		        });
		        jQuery(".portfolio-details-' . $pid . ' .portfolio-menu a").click(function(){
		            jQuery(".portfolio-details-' . $pid . ' .portfolio_details").html("");
		            var selector = jQuery(this).attr("data-filter");
		            jQuery(".portfolio-details-' . $pid . ' .portfolio-menu li.active").removeClass("active");
		            jQuery(this).parent().addClass("active");
		            isotopeContainer.isotope({ filter: selector });
		            return false;
		        });
		});
	</script>';
	return $output;
}
add_shortcode('button', 'teo_button');
function teo_button($atts, $content=null) {
	extract(shortcode_atts(array(
		'url' => '',
		'newwindow' => 'no',
	), $atts));
	
	if($newwindow != '')
		$target = ' target="_blank" ';
	else
		$target = '';
	if($content !== '')
	{
		if($url === '')
			$output = '<div class="custom-button">' . $content . '</div>';
		else
			$output = '<a ' . $target . ' href="' . esc_url($url) . '"><div class="custom-button">' . $content . '</div></a>';

		return $output;
	}
	else return '';
}


add_shortcode('clear', 'teo_clear');
function teo_clear($atts, $content=null) {
	return '<div class="clear"></div>';
}
add_shortcode('center', 'teo_centered');
function teo_centered($atts, $content=null) {
	
	return '<div style="text-align: center">' . do_shortcode($content) . '</div>';
}

add_shortcode('header','teo_header');
function teo_header($atts, $content = null) {
	
	$output = '<h3>' . do_shortcode($content) . '</h3>';
	return $output;
}
add_shortcode('subheader','teo_subheader');
function teo_subheader($atts, $content = null) {
	
	$output = '<p class="description">' . do_shortcode($content) . '</p>';
	return $output;
}
add_shortcode('team','teo_team');
function teo_team($atts, $content = null) {
	extract(shortcode_atts(array(
		"image" => '',
		"name" => '',
		"position" => '',
		"description" => '',
		"twitter" => '',
		"facebook" => '',
		"gplus" => '',
		"pinterest" => '',
		"columns" => 3
	), $atts));
	switch($columns)
	{
		case 1:
			$size = 'style="width: 100%"';
			break;
		case 2:
			$size = 'style="width: 50%"';
			break;
		case 3:
			$size = 'style="width: 33.3%"';
			break;
		case 4: 
			$size = 'style="width: 25%"';
			break;
		default:
			$size = 'style="width: 33.3%"';
			break;
	}
	$output = '<div class="team-member" ' . $size . '>';
	if($image !== '')
		$output .= '<figure>
			<img alt="' . esc_attr($name) . '" class="scale-with-grid" src="' . esc_attr($image) . '" />
		</figure>';
	if($name !== '')
		$output .= '<h4>' . esc_attr($name) . '</h4>';
	if($position !== '')
		$output .= '<span class="job-title">' . esc_attr($position) . '</span>';
	if($description !== '')
		$output .= '<p>' . esc_attr($description) . '</p>';
	$output .= '<div class="social-links"><ul>';
	if($twitter !== '')
		$output .= '<li><a target="_blank" href="' . esc_url($twitter) . '" class="twitter">Facebook</a></li>';
	if($facebook !== '')
		$output .= '<li><a target="_blank" href="' . esc_url($facebook) . '" class="facebook">Twitter</a></li>';
	if($gplus !== '')
		$output .= '<li><a target="_blank" href="' . esc_url($gplus) . '" class="googleplus">Google+</a></li>';
	if($pinterest !== '')
		$output .= '<li><a target="_blank" href="' . esc_url($pinterest) . '" class="pinterest">Pinterest</a></li>';
	$output .= '</ul>
	</div>
	</div>';
	return $output;
}
add_shortcode('service','teo_service');
function teo_service($atts, $content = null) {
	extract(shortcode_atts(array(
		"icon" => '',
		"title" => '',
		"text" => '',
		"columns" => '3'
	), $atts));
	switch($columns)
	{
		case 1:
			$class = 'class="span12 services-box"';
			break;
		case 2:
			$class = 'class="span6 services-box"';
			break;
		case 3:
			$class = 'class="span4 services-box"';
			break;
		case 4: 
			$class = 'class="span3 services-box"';
			break;
		default:
			$class = 'class="span4 services-box"';
			break;
	}
	$icon = esc_url($icon);
	$text = esc_attr($text);
	$title = esc_attr($title);
	$output = '<div ' . $class . '>';
	$output .= '<div class="services-diamond">
        <div class="content"></div>';
    if($icon != '')
    	$output .= '<div class="services-icon" style="background-image: url(\'' . $icon . '\')"></div>';
    else {
    	$output .= '<div class="services-icon"></div>';
    }
    $output .= '<div class="services-tail"></div>
        <div class="bottom-diamond"></div>
    </div>';
    $output .= '<div class="bottom">';
	if($title != '')
		$output .= '<h3>' . $title . '</h3>';
	if($text != '')
		$output .= '<h4>' . $text . '</h4>';
	$output .= '</div>
	</div>';
	return $output;
}

add_shortcode('gallery', 'teo_gallery');
function teo_gallery($atts, $content=null) {	
	$output = "<div class='flickr-stream'>";
	$output .= do_shortcode($content);
	$output .= '</div>
	<div style="clear: both"></div>';
	return $output;
}

add_shortcode('gallery_item', 'teo_gallery_item');
function teo_gallery_item($atts, $content=null) {
	extract(shortcode_atts(array(
		'thumb' => '',
		'alt' => '',
	), $atts));
	
	if($content != '')
	{
		if($thumb == '')
			$thumb = $content;
		$output = '<div class="flickr-item">
			<figure>';
		$output .= '<a rel="prettyPhoto[flickrGallery]" href="' . esc_url($content) . '" title="' . $alt . '"><img src="' . esc_url($thumb) . '" alt="' . $alt . '" /></a>';
		$output .= '</figure>
		</div>';
		return $output;
	}
	else return '';
}
?>