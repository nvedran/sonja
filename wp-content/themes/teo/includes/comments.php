<?php if ( ! function_exists( 'vp_comment' ) ) :
function vp_comment($comment, $args, $depth) {
   $GLOBALS['comment'] = $comment; ?>
   <li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
   <div id="comment-<?php comment_ID(); ?>" class="comment clearfix">
		<div class="left">
			<figure>
				<?php echo get_avatar($comment, 74); ?>
			</figure>
		</div>
				
		<div class="right">
			<div class="text">
		        <div class="name">
	          		<?php comment_author_link();?>
	          		<span class="post-date"><?php echo(get_comment_date('F jS, Y G:i')) ?></span>
	          	</div>
	        	<p>
	        		<?php comment_text();
		          	$reply_link = get_comment_reply_link( array_merge( $args, array('reply_text' => esc_attr__('Reply','Teo'),'depth' => $depth, 'max_depth' => $args['max_depth'])) ); 
		          	if ( $reply_link ) echo $reply_link;
		          	edit_comment_link( __( '(Edit)', 'Teo' ), ' ' ); ?>
		          </p>

         		<?php if ($comment->comment_approved == '0') : ?>
				<p>
					<em class="moderation"><?php esc_html_e('Your comment is awaiting moderation.','Teo') ?></em>
				</p>
				<br />
			<?php endif; ?>
        </div>
			
	</div> <!-- end comment-body-->
<?php }
endif; ?>