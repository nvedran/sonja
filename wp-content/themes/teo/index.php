<?php 
session_start();
global $teo_options;
if(isset($_POST['submit']))
{
    if( !$_POST['captcha'] || $_POST['captcha'] == '' || !$_POST['name'] || !$_POST['email'] || !$_POST['comment'] || $_POST['name'] == '' || $_POST['email'] == ''|| $_POST['comment'] == '')
    {
        echo 'Please fill in all the required fields';
    }
    elseif($_POST['captcha'] != $_SESSION['number1'] * $_SESSION['number2']) {
        echo 'Captcha is incorrect, please fill the form again.';
    } 
    else {
            $absolute_path = __FILE__;
            $path_to_file = explode( 'wp-content', $absolute_path );
            $path_to_wp = $path_to_file[0];

            // Access WordPress
            require_once( $path_to_wp . '/wp-load.php' );
            $teo_options = get_option('teo');
            $name = esc_html($_POST['name']);
            $email = esc_html($_POST['email']);
            $comment = esc_html($_POST['comment']);
            $msg = esc_attr('Name: ', 'Teo') . $name . PHP_EOL;
            $msg .= esc_attr('E-mail: ', 'Teo') . $email . PHP_EOL;
            $msg .= esc_attr('Message: ', 'Teo') . $comment;
            $to = $teo_options['email'];
            $sitename = get_bloginfo('name');
            $subject = '[' . $sitename . ']' . ' New Message';
            $headers = 'From: ' . $name . ' <' . $email . '>' . PHP_EOL;
            wp_mail($to, $subject, $msg, $headers);
            echo 'Your message has been sent';
    }
    die();
}
$_SESSION['number1'] = rand(1,10);
$_SESSION['number2'] = rand(1,10);
get_header(); ?>    
<div class='scrollable-container'>
        <section class='homepage-slider' id='home'>
            <ul class='slides'>
                <?php if(isset($teo_options['slideshow_img1']) && $teo_options['slideshow_img1'] != '') { ?>
                    <li style='background-image: url("<?php echo esc_url($teo_options['slideshow_img1']);?>")'></li>
                <?php } else { ?>
                    <li style='background-image: url("<?php echo get_template_directory_uri() . '/';?>img/slider01.jpg");'></li>
                <?php } 
                
                if(isset($teo_options['slideshow_img2']) && $teo_options['slideshow_img2'] != '') { ?>
                    <li style='background-image: url("<?php echo esc_url($teo_options['slideshow_img2']);?>")'></li>
                <?php }

                if(isset($teo_options['slideshow_img3']) && $teo_options['slideshow_img3'] != '') { ?>
                    <li style='background-image: url("<?php echo esc_url($teo_options['slideshow_img3']);?>")'></li>
                <?php }

                if(isset($teo_options['slideshow_img4']) && $teo_options['slideshow_img4'] != '') { ?>
                    <li style='background-image: url("<?php echo esc_url($teo_options['slideshow_img4']);?>")'></li>
                <?php } ?>
            </ul>
            <div class='text-section'>
                <div class='container'>
                    <div class='row'>
                        <div class='span12'>
                            <?php if(isset($teo_options['top_titletext']) && $teo_options['top_titletext'] != '') { ?>
                                <span class='title'><?php echo __($teo_options['top_titletext']);?></span>
                            <?php } ?>
                            
                            <?php if(isset($teo_options['top_secondarytext']) && $teo_options['top_secondarytext'] != '') { ?>
                                <span class='sub-title'><?php echo __($teo_options['top_secondarytext']);?></span>
                            <?php } ?>
                            
                            <?php if(isset($teo_options['button_text']) && $teo_options['button_text'] != '') { 
                                if(isset($teo_options['button_url']) && $teo_options['button_url'] ) { ?>
                                    <a target="_blank" href="<?php echo __(esc_url($teo_options['button_url']));?>" class='homepage-slider-button'><?php echo $teo_options['button_text'];?></a>
                                <?php } else { ?>
                                    <a class='homepage-slider-button'><?php echo __($teo_options['button_text']);?></a>
                                <?php }
                            } ?>  
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <div class="clear"> </div>

        <?php 
        if ( ( $locations = get_nav_menu_locations() ) && $locations['top-menu'] ) {
            $menu = wp_get_nav_menu_object( $locations['top-menu'] );
            $menu_items = wp_get_nav_menu_items($menu->term_id);
            $include = array();
            foreach($menu_items as $item) {
                if($item->object == 'page')
                    $include[] = $item->object_id;
            }
            query_posts( array( 'post_type' => 'page', 'post__in' => $include, 'posts_per_page' => count($include), 'orderby' => 'post__in' ) );
        }
        else
        {
            if(isset($teo_options['pages_topmenu']) && $teo_options['pages_topmenu'] != '' )
                query_posts(array( 'post_type' => 'page', 'post__in' => $teo_options['pages_topmenu'], 'posts_per_page' => count($teo_options['pages_topmenu']), 'orderby' => 'menu_order', 'order' => 'ASC' ) );
            else
                query_posts(array( 'post_type' => 'page', 'posts_per_page' => 4, 'orderby' => 'menu_order', 'order' => 'ASC' ) );
        }
        $i = 1;
        $j = 1; //count of the parallax separators
        while(have_posts() ) : the_post(); 
            $title = get_post_meta($post->ID, '_page_title', true);
            $description = get_post_meta($post->ID, '_page_description', true);
            $bgimage = esc_url(get_post_meta($post->ID, '_page_bgimage', true));
            $bgcolor = get_post_meta($post->ID, '_page_bgcolor', true);
            $color = get_post_meta($post->ID, '_page_color', true);
            $style = '';
            if($bgimage != '') 
                $style .= 'background-image: url(\'' . $bgimage . '\');';
            if($bgcolor != '' && $bgcolor != '#')
                $style .= ' background-color: ' . $bgcolor . ';';
            if($color != '' && $color != '#')
                $style .= ' color: ' . $color . ' !important; ';
            $template_file = get_post_meta($post->ID,'_wp_page_template',TRUE);
            ?>
            <section class='section-title' id='<?php echo $post->post_name;?>'>
                <div class='container'>
                    <div class='row'>
                        <div class='span12'>

                            <h1>
                                <?php if($title != '') echo $title; else the_title();?>
                            </h1>
                            <?php if($description != '') echo '<h2>' . $description . '</h2>';?>
                        </div>
                        <div class='section-diamond'></div>
                    </div>
                </div>
            </section>
            <section class='section-default <?php if($template_file == 'page-template-portfolio.php') echo 'portfolio-slider'; if($template_file == 'page-template-blog.php') echo ' section-blog';?>' id='<?php echo $post->post_name;?>_section' style="<?php echo $style;?>">
                <div class='container'>
                    <div class='row'>
                        <?php
                        if($template_file == 'page-template-blog.php') {
                            //blog
                            $settings = get_post_meta($post->ID, 'vp_ptemplate_settings', true);
                            $pageid = $post->ID;
                            $args = array();
                            if($settings['categories'] != '')
                                $args['category__in'] = $settings['categories'];
                            $args['posts_per_page'] = 3;
                            $query = new WP_Query($args);
                            $i = 1;
                            echo '<div class="span6 offset3">';
                            while($query->have_posts() ) : $query->the_post(); ?>
                                    <?php if($i==1) { ?>
                                        <article class='featured'>
                                            <?php if(has_post_thumbnail() ) { 
                                                $thumb = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );?>
                                                <figure>
                                                    <img src="<?php echo aq_resize($thumb, 770, 352, true);?>" alt="<?php the_title();?>" />
                                                    <a href="<?php the_permalink();?>" class='figure-link'>
                                                        <i class='plus-sign'></i>
                                                    </a>
                                                </figure>
                                            <?php } ?>
                                             <h2>
                                                <a href="<?php the_permalink();?>"><?php the_title();?></a>
                                            </h2>
                                            <span class='date'><?php the_time("d M, Y");?> - <?php _e('By ', 'Teo'); the_author_posts_link();?></span>
                                            <div class='excerpt'>
                                               <?php the_excerpt();?>
                                            </div>
                                        </article>
                                    <?php } else {
                                        if($i==2) { ?>
                                            <div class='row'>
                                        <?php } ?>
                                                <article class='span3'>
                                                    <?php if(has_post_thumbnail() ) { 
                                                        $thumb = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );?>
                                                        <figure>
                                                            <img src="<?php echo aq_resize($thumb, 470, 353, true);?>" alt="<?php the_title();?>" />
                                                            <a href="<?php the_permalink();?>" class='figure-link'>
                                                                <i class='plus-sign'></i>
                                                            </a>
                                                        </figure>
                                                    <?php } ?>
                                                    <h2>
                                                        <a href="<?php the_permalink();?>"><?php the_title();?></a>
                                                    </h2>
                                                    <span class='date'><?php the_time("d M, Y");?> - <?php _e('By ', 'Teo'); the_author_posts_link();?></span>
                                                    <div class='excerpt'>
                                                        <?php the_excerpt();?>
                                                    </div>
                                                </article>
                                        <?php if($i == 3) { ?>
                                                <div class="span6 button-container">
                                                    <a class="btn custom-button" href="<?php echo get_permalink($pageid);?>"><?php _e('View all blog posts', 'Teo'); ?></a>
                                                </div>
                                            </div>
                                        <?php } 
                                        } ?>
                            <?php $i++; endwhile; wp_reset_postdata(); 
                            if($i == 3) echo '</div>';
                            echo '</div>';
                        } else {
                            //not blog, we show the pages normally
                            $type = get_post_meta($post->ID, '_page_type', true);
                            global $more; $more = 0;
                            if($type == 1 || $template_file == 'page-template-portfolio.php' || $template_file == 'page-template-services.php') 
                                the_content('');
                            else
                                echo '<div class="span10 offset1">' . do_shortcode(get_the_content('')) . '</div>';
                        } ?>
                            </div>
                    </div>
            </section>
            <!-- separator image and a slider with quotes -->        
            <?php
            $image = get_post_meta($post->ID, '_separator_bgimage', true);
            $quote1 = get_post_meta($post->ID, '_separator_quote1', true);
            $quote1_author = get_post_meta($post->ID, '_separator_quote1_author', true);
            $quote2 = get_post_meta($post->ID, '_separator_quote2', true);
            $quote2_author = get_post_meta($post->ID, '_separator_quote2_author', true);
            $quote3 = get_post_meta($post->ID, '_separator_quote3', true);
            $quote3_author = get_post_meta($post->ID, '_separator_quote3_author', true);
            $quote4 = get_post_meta($post->ID, '_separator_quote4', true);
            $quote4_author = get_post_meta($post->ID, '_separator_quote4_author', true);
            if($image != '' && $quote1 != '') { ?>
                <section class='section-parallax' id="section-parallax-<?php echo $j; ?>">
                    <div class='bg-image' style="background: url('<?php echo esc_url($image);?>') 50% 0 no-repeat fixed">
                        <div class='container'>
                            <div class='row'>
                                <div class='span12 quote-slider'>
                                    <ul class='slides'>
                                        <li>
                                            <div class='row'>
                                                <div class='span6 offset3'>
                                                    <span class='quote'>
                                                        <span class='left-quote'>&ldquo;</span>
                                                        <?php echo $quote1;?>
                                                        <span class='right-quote'>&bdquo;</span>
                                                    </span>
                                                    <?php if($quote1_author != '') { ?>
                                                        <span class='quote-author'>
                                                            <?php echo $quote1_author;?>
                                                        </span>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </li>

                                        <?php if($quote2 != '') { ?>
                                            <li>
                                                <div class='row'>
                                                    <div class='span6 offset3'>
                                                        <span class='quote'>
                                                            <span class='left-quote'>&ldquo;</span>
                                                            <?php echo $quote2;?>
                                                            <span class='right-quote'>&bdquo;</span>
                                                        </span>
                                                        <?php if($quote2_author != '') { ?>
                                                            <span class='quote-author'>
                                                                <?php echo $quote2_author;?>
                                                            </span>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </li>
                                        <?php } ?>

                                        <?php if($quote3 != '') { ?>
                                            <li>
                                                <div class='row'>
                                                    <div class='span6 offset3'>
                                                        <span class='quote'>
                                                            <span class='left-quote'>&ldquo;</span>
                                                            <?php echo $quote3;?>
                                                            <span class='right-quote'>&bdquo;</span>
                                                        </span>
                                                        <?php if($quote3_author != '') { ?>
                                                            <span class='quote-author'>
                                                                <?php echo $quote3_author;?>
                                                            </span>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </li>
                                        <?php } ?>

                                        <?php if($quote4 != '') { ?>
                                            <li>
                                                <div class='row'>
                                                    <div class='span6 offset3'>
                                                        <span class='quote'>
                                                            <span class='left-quote'>&ldquo;</span>
                                                            <?php echo $quote4;?>
                                                            <span class='right-quote'>&bdquo;</span>
                                                        </span>
                                                        <?php if($quote4_author != '') { ?>
                                                            <span class='quote-author'>
                                                                <?php echo $quote4_author;?>
                                                            </span>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            <?php $j++; }
            
            $i++; endwhile; wp_reset_postdata(); ?>

        <section class='section-title' id='contact'>
            <div class='container'>
                <div class='row'>
                    <div class='span12'>
                        <h1><?php _e('Contact', 'Teo');?></h1>
                        <?php if(isset($teo_options['contact_description']) && $teo_options['contact_description'] != '') { ?>
                            <h2><?php echo $teo_options['contact_description'];?></h2>
                        <?php } ?>
                    </div>
                    <div class='section-diamond'></div>
                </div>
            </div>
        </section>
        <section class='section-default section-contact'>
            <div class='container'>
                <div class='row'>
                    <div class='span10 offset1'>
                        <div class='row'>
                            <div class='span6'>
                                <div class="confirmation_message">
                                    <h3><?php _e('Your message has been sent', 'Teo');?></h3>
                                </div>
                                <form action="" class='custom-form dark-form'>
                                    <label for="contact-name"><?php _e('Name:', 'Teo');?></label>
                                    <input id='contact-name' type="text" name='name' class='span6' placeholder="<?php _e('Your name here...', 'Teo');?>" />
                                    <label for="contact-email"><?php _e('Email:', 'Teo');?></label>
                                    <input id='contact-email' type="email" name='email' class='span6' placeholder="<?php _e('Your email here...', 'Teo');?>" />
                                    <label for="contact-captcha"><?php _e('Captcha: ', 'Teo');?><?php echo $_SESSION['number1'] . '*' . $_SESSION['number2'] . ' = ';?></label>
                                    <input id='contact-captcha' type="text" name='captcha' class='span6' placeholder="<?php _e('The captcha answer...', 'Teo');?>" />
                                    <label for="contact-message"><?php _e('Message:', 'Teo');?></label>
                                    <textarea id='contact-message' name="message" class='span6' rows="5" placeholder="<?php _e('Your message here...', 'Teo');?>"></textarea>
                                    <input id="send_message" type="submit" name='submit' value='<?php _e('Send message', 'Teo');?>' class='btn custom-button'/>
                                </form>
                            </div>
                            <div class='span4'>
                                <h3><?php _e('Contact Info', 'Teo');?></h3>
                                <?php if(isset($teo_options['contact_descriptiontext']) && $teo_options['contact_descriptiontext'] != '') { ?>
                                    <p class='description'>
                                        <?php echo $teo_options['contact_descriptiontext'];?>
                                    </p>
                                <?php } ?>
                                
                                <?php if(isset($teo_options['address']) && $teo_options['address'] != '') { ?>
                                    <address>
                                        <?php echo $teo_options['address'];?>
                                    </address>
                                <?php } ?>

                                <?php if(isset($teo_options['contact_email']) && $teo_options['contact_email'] != '') { ?>
                                    <address>
                                        <?php _e('E-mail: ', 'Teo'); echo encEmail($teo_options['contact_email']);?>
                                    </address>
                                <?php } ?>

                                <?php if(isset($teo_options['phone']) && $teo_options['phone'] != '') { ?>
                                    <address>
                                        <?php _e('Phone Number: ', 'Teo'); echo $teo_options['phone'];?>
                                    </address>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    
<?php get_footer();?>