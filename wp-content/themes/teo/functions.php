<?php
$teo_options = get_option('teo');
add_action( 'after_setup_theme', 'teo_setup' );
if ( ! function_exists( 'teo_setup' ) ){
	function teo_setup(){
		global $teo_options;
		require get_template_directory() . '/teoPanel/custom-functions.php';
		require get_template_directory() . '/includes/shortcodes.php';
		require get_template_directory() . '/includes/comments.php';
		require get_template_directory() . '/includes/widgets.php';
		require get_template_directory() . '/teoPanel/shortcodes/shortcodes.php';
		require get_template_directory() . '/includes/additional_functions.php';
		require get_template_directory() . '/teoPanel/post_types.php';
		require get_template_directory() . '/teoPanel/custom_metaboxes/meta_boxes.php';
		load_theme_textdomain('Teo', get_template_directory() . '/languages');
		require 'teoPanel/nhp-options.php';
	}
}
// Loading js files into the theme
add_action('wp_head', 'teo_scripts');
if ( !function_exists('teo_scripts') ) {
	function teo_scripts() {
		global $teo_options;
		wp_enqueue_script( 'flexslider', get_template_directory_uri() . '/js/jquery.flexslider.js', array(), '1.0');
		wp_enqueue_script( 'isotope', get_template_directory_uri() . '/js/jquery.isotope.min.js', array(), '1.0');
		wp_enqueue_script( 'tinynav', get_template_directory_uri() . '/js/tinynav.min.js', array(), '1.0');
		wp_enqueue_script( 'prettyPhoto', get_template_directory_uri() . '/js/jquery.prettyPhoto.js', array(), '1.0');
		wp_enqueue_script( 'jquery-placeholder', get_template_directory_uri() . '/js/jquery.placeholder.min.js', array(), '1.0');
		wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array(), '1.0');
		wp_enqueue_script( 'main', get_template_directory_uri() . '/js/main.js', array(), '1.0');
		if ( is_singular() && get_option( 'thread_comments' ) )
    		wp_enqueue_script( 'comment-reply' );
	}

}

//Loading the CSS files into the theme
add_action('wp_enqueue_scripts', 'teo_load_css');
if( !function_exists('teo_load_css') ) {
	function teo_load_css() {
		global $teo_options;
		wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.css', array(), '1.0');
		wp_enqueue_style( 'bootstrap-responsive', get_template_directory_uri() . '/css/bootstrap-responsive.css', array(), '1.0');
		wp_enqueue_style( 'flexslider', get_template_directory_uri() . '/css/flexslider.css', array(), '1.0');
		wp_enqueue_style( 'prettyPhoto', get_template_directory_uri() . '/css/prettyPhoto.css', array(), '1.0');
		wp_enqueue_style( 'style', get_stylesheet_directory_uri() . '/style.css', array(), '1.0');
		if(isset($teo_options['color_scheme']) && $teo_options['color_scheme'] == 2)
			wp_enqueue_style( 'light-theme', get_template_directory_uri() . '/css/light-theme.css', array(), '1.0');
		else 
			wp_enqueue_style( 'dark-theme', get_template_directory_uri() . '/css/dark-theme.css', array(), '1.0');
		wp_enqueue_style( 'open-sans', 'http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,800,700,600|Oswald:400,300,700', array());
		wp_enqueue_style( 'droid-serif', 'http://fonts.googleapis.com/css?family=Droid+Serif:400italic', array());
		wp_enqueue_script('jquery');
		wp_enqueue_script( 'flexslider', get_template_directory_uri() . '/js/jquery.flexslider.js', array(), '1.0');
	}
}

add_action('wp_head', 'teo_custom_css', 11);
function teo_custom_css() {
	global $teo_options;
	if(isset($teo_options['custom_css']) && $teo_options['custom_css'] != '')
			echo '<style type="text/css">' . $teo_options['custom_css'] . '</style>';
}

add_action('init', 'teo_misc');
function teo_misc() {
	global $teo_options;
	add_theme_support( 'post-thumbnails');
	add_theme_support( 'automatic-feed-links' );
	
}
if ( ! isset( $content_width ) ) $content_width = 960;

function encEmail ($orgStr) {
    $encStr = "";
    $nowStr = "";
    $rndNum = -1;

    $orgLen = strlen($orgStr);
    for ( $i = 0; $i < $orgLen; $i++) {
        $encMod = rand(1,2);
        switch ($encMod) {
        case 1: // Decimal
            $nowStr = "&#" . ord($orgStr[$i]) . ";";
            break;
        case 2: // Hexadecimal
            $nowStr = "&#x" . dechex(ord($orgStr[$i])) . ";";
            break;
        }
        $encStr .= $nowStr;
    }
    return $encStr;
} 

function register_menus() {
	register_nav_menus( array( 'top-menu' => 'Top primary menu')
						);
}
add_action('init', 'register_menus');

class description_walker extends Walker_Nav_Menu
{
      function start_el(&$output, $item, $depth=0, $args=array(), $id=0)
      {
           global $wp_query;
           $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

           $class_names = $value = '';

           $classes = empty( $item->classes ) ? array() : (array) $item->classes;

           $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
           $class_names = ' class="'. esc_attr( $class_names ) . '"';

           $output .= $indent . '<li id="menu-item-'. $item->ID . '"' . $value . $class_names .'>';

           $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
           $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
           $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
           if($item->object == 'page')
           {
                $varpost = get_post($item->object_id);
                $attributes .= ' data-target="#' . $varpost->post_name . '" href="' . get_site_url() . '/#' . $varpost->post_name . '"';
           }
           else {
           		if (strpos($item->url,'#home') !== false) {
           			$attributes .= ! empty( $item->url )        ? ' data-target="#home" href="'   . esc_attr( $item->url        ) .'"' : '';
           		}
           		elseif (strpos($item->url,'#contact') !== false) {
           			$attributes .= ! empty( $item->url )        ? ' data-target="#contact" href="'   . esc_attr( $item->url        ) .'"' : '';
           		}
           		else
                	$attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
           }
           	$item_output = $args->before;
            $item_output .= '<a'. $attributes .'>';
            $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID );
            $item_output .= $args->link_after;
            $item_output .= '</a>';
            $item_output .= $args->after;

            $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args, $id );
            }
}
add_filter( 'posts_orderby', 'sort_query_by_post_in', 10, 2 );
function sort_query_by_post_in( $sortby, $thequery ) {
	if ( !empty($thequery->query['post__in']) && isset($thequery->query['orderby']) && $thequery->query['orderby'] == 'post__in' )
		$sortby = "find_in_set(ID, '" . implode( ',', $thequery->query['post__in'] ) . "')";
	return $sortby;
}

add_action('init', 'teo_sidebars');
function teo_sidebars() {
	$args = array(
				'name'          => 'Right sidebar',
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<div class="title-container"><h2 class="widgettitle">',
				'after_title'   => '</h2></div>' );
	register_sidebar($args);
}
?>