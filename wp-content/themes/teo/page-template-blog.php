<?php
/* 
Template name: Blog page template
*/
get_header();
global $teo_options;
the_post(); 
$options = get_post_meta($post->ID, 'vp_ptemplate_settings', true);
$fullwidth = isset($options['fullwidth']) ? $options['fullwidth'] : '1';
$description = get_post_meta($post->ID, '_page_description', true);
?>
<div class='scrollable-container'>
    <section class='section-title' id='blog-page'>
        <div class='container'>
            <div class='row'>
                <div class='span12'>
                    <h1><?php the_title();?></h1>
                    <?php if($description != '') echo '<h2>' . $description . '</h2>';?>
                </div>
                <div class='section-diamond'></div>
            </div>
        </div>
    </section>
    <section class='section-default section-blog'>
        <div class='container'>
            <div class='row'>
                <div class="span<?php if($fullwidth == 1) echo '12'; else echo '8';?>">
                    <div class='inner'>
                        <?php
                        $args = array();
                        if($options['categories'] != '')
                            $args['category__in'] = $options['categories'];
                        $args['posts_per_page'] = (int)$options['blog_posts'] != 0 ? (int)$options['blog_posts'] : 4;
                        $paged = is_front_page() ? get_query_var( 'page' ) : get_query_var( 'paged' );
                        $args['paged'] = $paged;
                        query_posts($args);
                        $i=1;
                        if(have_posts() ) : while(have_posts() ) : the_post(); ?>
                            <article <?php if($i==1) post_class("first-article"); else post_class(); ?>>
                                <div class='inner-article'>
                                    <?php if(has_post_thumbnail() ) { 
                                        $thumb = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );?>
                                        <figure>
                                            <?php if($fullwidth == 1) {
                                                $width = 1000;
                                                $height = 420;
                                            }
                                            else {
                                                $width = 641;
                                                $height = 320;
                                            }
                                            ?>
                                            <img src="<?php echo aq_resize($thumb, $width, $height, true);?>" alt="<?php the_title();?>" />
                                            <a href="<?php the_permalink();?>" class='figure-link'><i class='plus-sign'></i></a>
                                        </figure>
                                    <?php } ?>
                                    <h2><a href='<?php the_permalink();?>'><?php the_title();?></a></h2>
                                    <span class='date'><?php the_time("d M, Y");?> - <?php _e('By ', 'Teo'); the_author_posts_link();?></span>
                                    <div class='excerpt'>
                                        <p><?php the_excerpt();?></p>
                                    </div>
                                    <a href="<?php the_permalink();?>" class='btn custom-button'><?php _e('Read more', 'Teo');?></a>
                                </div>
                            </article>
                        <?php $i++; endwhile; get_template_part('includes/pagination'); endif; wp_reset_query(); ?>
                    </div>
                </div>

                <?php if($fullwidth == 0) { ?>
                    <aside class='span4'>
                        <?php dynamic_sidebar("Right sidebar"); ?>
                    </aside>
                <?php } ?>
            </div>
        </div>
    </section>

<?php get_footer();?>