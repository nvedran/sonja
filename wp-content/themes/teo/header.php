<?php global $teo_options; ?>
<!DOCTYPE html>
<!--[if lt IE 7 ]><html style="margin-top: 0 !important" class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html style="margin-top: 0 !important" class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html style="margin-top: 0 !important" class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--> <!--<![endif]-->
<html style="margin-top: 0 !important" <?php language_attributes(); ?>> <!--<![endif]-->
<head>
    <meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
    <title><?php wp_title('-');?></title>
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
    <!-- Mobile Specific Metas
      ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    
    <script type="text/javascript">
    var templateDir = "<?php echo get_template_directory_uri(); ?>";
    </script>


    <?php global $teo_options;  
    if(isset($teo_options['integration_header'])) echo $teo_options['integration_header'] . PHP_EOL;
    wp_head(); ?>
</head>
<body data-spy="scroll" data-target="#mainNav" data-offset="100" <?php body_class();?>>

<header>
        <div class='container'>
            <div class='row'>
                <div class='span3'>
                    <figure class='logo'>
                        <a href="<?php echo home_url();?>/#home">
                            <?php if(isset($teo_options['logo']) && $teo_options['logo'] != '') { ?>
                                <img src="<?php echo esc_url($teo_options['logo']);?>" alt="<?php bloginfo('name');?>" />
                            <?php } else { ?>
                                <img src="<?php echo get_template_directory_uri() . '/';?>img/logo.png" alt="<?php bloginfo('name');?>"/>
                            <?php } ?>
                        </a>
                    </figure>
                </div>
                <nav class='span6'>
                    <div id='mainNav'>
                        <?php wp_nav_menu(array(
                                'theme_location' => 'top-menu',
                                'menu_class' => 'nav',
                                'container' => '',
                                'fallback_cb' => 'show_top_menu',
                                'echo' => true,
                                'walker' => new description_walker(),
                                'depth' => 1 ) ); ?>
                        <div class='active-indicator hidden-phone'></div>
                    </div>
                </nav>
                <div class='span3'>
                    <ul class='social-icons'>
                        <?php if(isset($teo_options['facebook_url']) && $teo_options['facebook_url'] != '') { ?>
                            <li><a href="<?php echo $teo_options['facebook_url'];?>" class='social facebook'></a></li>
                        <?php } ?>
                        
                        <?php if(isset($teo_options['twitter_url']) && $teo_options['twitter_url'] != '') { ?>
                            <li><a href="<?php echo $teo_options['twitter_url'];?>" class='social twitter'></a></li>
                        <?php } ?>
                        
                        <?php if(isset($teo_options['gplus_url']) && $teo_options['gplus_url'] != '') { ?>
                            <li><a href="<?php echo $teo_options['gplus_url'];?>" class='social googleplus'></a></li>
                        <?php } ?>

                        <?php if(isset($teo_options['digg_url']) && $teo_options['digg_url'] != '') { ?>
                            <li><a href="<?php echo $teo_options['digg_url'];?>" class='social digg'></a></li>
                        <?php } ?>
                       
                        <?php if(isset($teo_options['pinterest_url']) && $teo_options['pinterest_url'] != '') { ?>
                            <li><a href="<?php echo $teo_options['pinterest_url'];?>" class='social pinterest'></a></li>
                        <?php } ?>

                        <?php if(isset($teo_options['vimeo_url']) && $teo_options['vimeo_url'] != '') { ?>
                            <li><a href="<?php echo $teo_options['vimeo_url'];?>" class='social vimeo'></a></li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
    </header>