
;(function($){
    $(function(){
       
        var $sidebar_content = $('#owlabbulkg-config-slider-sidebar-content-box').hide(),
            $same_ratio = $('#owlabbulkg-config-slider-ratio-box').hide(),
            $cols = $('#owlabbulkg-config-slider-col-box').hide(),
            $nopadding = $('#owlabbulkg-config-slider-nopadding-box').hide(),
            $icon = $('#owlabbulkg-config-slider-icon-box').hide();

        $(document).ready(function(){
            if ( $('select[name="_owlabbulkg[layout]"]').val() == 'grid'){
                $same_ratio.fadeIn();
                $cols.fadeIn();
                $nopadding.fadeIn();
            }

            if( $('select[name="_owlabbulkg[hover]"]').val() == 'simple-icon'){
                $icon.fadeIn();
            }

            if ( $('input[name="_owlabbulkg[sidebar]"]').is(':checked')){
                $sidebar_content.fadeIn();
            }
        });

        $(document).on('change','select[name="_owlabbulkg[layout]"]',function(){

            if ( this.value != 'grid'){
                $same_ratio.fadeOut();
                $cols.fadeOut();
                $nopadding.fadeOut();
            }else{
                $same_ratio.fadeIn();
                $cols.fadeIn();
                $nopadding.fadeIn();
            }
        });

        $(document).on('change','input[name="_owlabbulkg[sidebar]"]',function(){

            if ( $(this).is(':checked')){
                $sidebar_content.fadeIn();
            }else{
                $sidebar_content.fadeOut();
            }
        });
        
        $(document).on('change','select[name="_owlabbulkg[hover]"]',function(){

            if ( this.value != 'simple-icon'){
                $icon.fadeOut();
            }else{
                $icon.fadeIn();
            }
        });   



        
        // Make gallery images sortable.
        var slider = $('#owlabbulkg-output');

        // Use ajax to make the images sortable.
        slider.sortable({
            containment: '#owlabbulkg',
            items: 'li',
            cursor: 'move',
            forcePlaceholderSize: true,
            placeholder: 'dropzone',
            update: function(event, ui) {
                // Make ajax request to sort out items.
                var opts = {
                    url:      owlabbulkg_metabox.ajax,
                    type:     'post',
                    async:    true,
                    cache:    false,
                    dataType: 'json',
                    data: {
                        action:  'owlabbulkg_sort_images',
                        order:   slider.sortable('toArray').toString(),
                        post_id: owlabbulkg_metabox.id,
                        nonce:   owlabbulkg_metabox.sort
                    },
                    success: function(response) {
                        return;
                    },
                    error: function(xhr, textStatus ,e) {
                        return;
                    }
                };
                $.ajax(opts);
            }
        });

        // Process image removal from a gallery.
        $(document).on('click', '#owlabbulkg .owlabbulkg-remove-slide', function(e){
            e.preventDefault();

            // Bail out if the user does not actually want to remove the image.
            var confirm_delete = confirm(owlabbulkg_metabox.remove);
            if ( ! confirm_delete )
                return;

            // Prepare our data to be sent via Ajax.
            var attach_id = $(this).parent().attr('id'),
                remove = {
                    action:        'owlabbulkg_remove_slide',
                    attachment_id: attach_id,
                    post_id:       owlabbulkg_metabox.id,
                    nonce:         owlabbulkg_metabox.remove_nonce
                };

            // Process the Ajax response and output all the necessary data.
            $.post(
                owlabbulkg_metabox.ajax,
                remove,
                function(response) {
                    $('#' + attach_id).fadeOut('normal', function() {
                        $(this).remove();

                        // Refresh the modal view to ensure no items are still checked if they have been removed.
                        $('.owlabbulkg-load-library').attr('data-owlabbulkg-offset', 0).addClass('has-search').trigger('click');
                    });
                },
                'json'
            );
        });

        // Open up the media modal area for modifying gallery metadata.
        var owlabbulkg_main_frame_meta = false;
        $(document).on('click.owlabbulkgModify', '#owlabbulkg .owlabbulkg-modify-slide', function(e){
            e.preventDefault();
            var attach_id = $(this).parent().data('owlabbulkg-slide'),
                formfield = 'owlabbulkg-meta-' + attach_id;

            // Show the modal.
            owlabbulkg_main_frame_meta = true;
            $('#' + formfield).appendTo('body').show();

           

            // Close the modal window on user action
            var append_and_hide_meta = function(e){
                e.preventDefault();
                $('#' + formfield).appendTo('#' + attach_id).hide();
                owlabbulkg_main_frame_meta = false;
                $(document).off('click.owlabbulkgLink');
            };
            $(document).on('click.owlabbulkgIframe', '.media-modal-close, .media-modal-backdrop', append_and_hide_meta);
            $(document).off('keydown.owlabbulkgIframe').on('keydown.owlabbulkgIframe', function(e){
                if ( 27 == e.keyCode && owlabbulkg_main_frame_meta ) {
                    append_and_hide_meta(e);
                }
            });
            $(document).on('click.owlabbulkgLink', '.ed_button', function(){
                // Set custom z-index for link dialog box.
                $('#wp-link-backdrop').css('zIndex', '170100');
                $('#wp-link-wrap').css('zIndex', '171005' );
            });
        });

        // Save the image metadata.
        $(document).on('click', '.owlabbulkg-meta-submit', function(e){
            e.preventDefault();
            var $this     = $(this),
                default_t = $this.text(),
                attach_id = $this.data('owlabbulkg-item'),
                formfield = 'owlabbulkg-meta-' + attach_id,
                meta      = {};

            // Output saving text...
            $this.text(owlabbulkg_metabox.saving);

            
            // Get all meta fields and values.
            $('#owlabbulkg-meta-table-' + attach_id).find(':input').not('.ed_button').each(function(i, el){
                if ( $(this).data('owlabbulkg-meta') )
                    meta[$(this).data('owlabbulkg-meta')] = $(this).val();
            });

            // modify the grid sizer
            meta.grid_sizer = 'off';
            if ( $('#owlabbulkg-meta-table-' + attach_id).find('[name="_owlabbulkg[meta_grid_sizer]"]').is(":checked") ){
                meta.grid_sizer = 'on';
            }
            

            // Prepare the data to be sent.
            var data = {
                action:    'owlabbulkg_save_meta',
                nonce:     owlabbulkg_metabox.save_nonce,
                attach_id: attach_id,
                post_id:   owlabbulkg_metabox.id,
                meta:      meta
            };

            $.post(
                owlabbulkg_metabox.ajax,
                data,
                function(res){
                    setTimeout(function(){
                        $('#' + formfield).appendTo('#' + attach_id).hide();
                        $this.text(default_t);
                    }, 500);
                },
                'json'
            );
        });

        
    });
}(jQuery));