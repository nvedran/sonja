<?php
/**
 * Common class.
 *
 * @since 1.0.0
 *
 * @package owwwlab-kenburn
 * @author  owwwlab
 */
class Owlabbulkg_Common {

    /**
     * Holds the class object.
     *
     * @since 1.0.0
     *
     * @var object
     */
    public static $instance;

    /**
     * Path to the file.
     *
     * @since 1.0.0
     *
     * @var string
     */
    public $file = __FILE__;

    /**
     * Holds the base class object.
     *
     * @since 1.0.0
     *
     * @var object
     */
    public $base;

    /**
     * Primary class constructor.
     *
     * @since 1.0.0
     */
    public function __construct() {

        // Load the base class object.
        $this->base = Owlabbulkg::get_instance();

    }



    /**
     * Helper method for setting default config values.
     *
     * @since 1.0.0
     *
     * @global int $id      The current post ID.
     * @global object $post The current post object.
     * @param string $key   The default config key to retrieve.
     * @return string       Key value on success, false on failure.
     */
    public function get_config_default( $key ) {

        global $id, $post;

        // Get the current post ID. If ajax, grab it from the $_POST variable.
        if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
            $post_id = absint( $_POST['post_id'] );
        } else {
            $post_id = isset( $post->ID ) ? $post->ID : (int) $id;
        }

        // Prepare default values.
        $defaults = $this->get_config_defaults( $post_id );

        // Return the key specified.
        return isset( $defaults[$key] ) ? $defaults[$key] : false;

    }

    /**
     * Retrieves the slider config defaults.
     *
     * @since 1.0.0
     *
     * @param int $post_id The current post ID.
     * @return array       Array of slider config defaults.
     */
    public function get_config_defaults( $post_id ) {

        $defaults = array(
            'type'            => 'default',
            'hover'           => 'simple-icon',
            'icon'            => 'fa-camera',
            'layout'          => 'grid',
            'same_ratio'      => NULL,
            'sidebar'         => NULL,
            'sidebar_content' => __("Chang me or delete me",'owlabbulkg'),
            'lg_col'          => 5,
            'md_col'          => 4,
            'sm_col'          => 3,
            'xs_col'          => 2,
            'nopadding'       => NULL,
            'ratio'           => 1,
            'grid_sizer'      => NULL,
            'short_des'       => __("Chang me or delete me",'owlabbulkg'),
        );
        return apply_filters( 'owlabbulkg_defaults', $defaults, $post_id );

    }




    /**
     * Helper method to flush slider caches once a slider is updated.
     *
     * @since 1.0.0
     *
     * @param int $post_id The current post ID.
     * @param string $slug The unique slider slug.
     */
    public function flush_slider_caches( $post_id, $slug = '' ) {

        // Delete known slider caches.
        delete_transient( '_sol_cache_' . $post_id );
        delete_transient( '_sol_cache_all' );

        // Possibly delete slug slider cache if available.
        if ( ! empty( $slug ) ) {
            delete_transient( '_sol_cache_' . $slug );
        }

        // Run a hook for Addons to access.
        do_action( 'owlabbulkg_flush_caches', $post_id, $slug );

    }

    /**
     * Helper method to return the max execution time for scripts.
     *
     * @since 1.0.0
     *
     * @param int $time The max execution time available for PHP scripts.
     */
    public function get_max_execution_time() {

        $time = ini_get( 'max_execution_time' );
        return ! $time || empty( $time ) ? (int) 0 : $time;

    }

    /**
     * Returns the singleton instance of the class.
     *
     * @since 1.0.0
     *
     * @return object The owlabbulkg_Common object.
     */
    public static function get_instance() {

        if ( ! isset( self::$instance ) && ! ( self::$instance instanceof Owlabbulkg_Common ) ) {
            self::$instance = new Owlabbulkg_Common();
        }

        return self::$instance;

    }

}

// Load the common class.
$owlabbulkg_Common = Owlabbulkg_Common::get_instance();





