<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'sonja_wpdb');

/** MySQL database username */
define('DB_USER', 'wpuser');

/** MySQL database password */
define('DB_PASSWORD', 'guja9y3yp');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '5E~d&8E7BFvdC{~OGId7v$sOzfIo{:^9^EbTrr3V>lNw>K?GX4RI@|oPZ.[XOZ`D');
define('SECURE_AUTH_KEY',  'P8<[,ydSDKrKbleN< ]v$P5~{W0r28EsZ[mKXk@:X.*v]srkD (th-oEUd}x-c6H');
define('LOGGED_IN_KEY',    'I4mN,xJ/O?`&,/bn;FfO45;]@p9l3PHOqP[.JlTF8JtqFI -4jK)`nU6cAhPWHaz');
define('NONCE_KEY',        'Tc;e5tviH*UW%7&ZpFth#}Co!!>Stl*=X$)G@|fwB;^ g^aejiW= /YlsUmi+JQA');
define('AUTH_SALT',        'IPVM4G:K%7X#K/6mu$:zM=EyvTDR.|V~a3rX>sc?@VF/jJh=B fRGaE8hCx]{Ca=');
define('SECURE_AUTH_SALT', '.q9k^l8l{dCp$`^De(orL9eMb]vy^[-@Qx1Vy1d-10GroSQ~WtcNr5r+q]bKOaKt');
define('LOGGED_IN_SALT',   's2 J^xnqv-r^2@GT|$:E4v@Fp|IaZ+IQ~l8wsvwR=!A{;UmSq(%B#OZ5V2<0!057');
define('NONCE_SALT',       'S*(CJ+l9d*UA|L!i+{_T`YxA7`2qLZDjKzTizcCJzjSv4;dC/t^2*ic$8!GzYk$p');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
